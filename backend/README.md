# Building REST API with Node and Express 4

## Introduction

- In this tutorial we will be developing simple REST API for movies collection with their released date. We gonna implement simple CRUD(Create, Read, Update and Delete) operations on movie collection data in our journey.
- Refer medium article for complete [tutorial](https://medium.com/@bhanushali.mahesh3/building-a-restful-crud-api-with-node-js-jwt-bcrypt-express-and-mongodb-4e1fb20b7f3d)

## Requirements

- Node, NPM & MongoDB (ip address must be in white list on server)

## Installation

- Install dependecies: `npm install`

## Start

- add .env file with this envirements for start project

admin_email = 'momotv678@gmail.com'
secret_key = 'diplom'
base_url = 'http://localhost/'
front_url = 'http://localhost:3000/'
rozetka_url = 'https://rozetka.com.ua/api/product-api/v4/goods/'

PORT=3001
DB_USER=vladmomot
DB_PASS=v8l5a2d3

- Start project on 3001 port: `npm start`
