const caseModel = require('../models/cases');

module.exports = {
  create: async function (req, res, next) {
    var newCase;
    try {
      newCase = await caseModel.create(req.body);
      res.json({ status: 'success', message: 'Case created successful', data: newCase });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedCase = req.body;
    delete updatedCase._id;

    try {
      updatedCase = await caseModel.findByIdAndUpdate(_id, updatedCase, { new: true });
      res.json({ status: 'success', message: 'Case updated successful', data: updatedCase });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var coolerWidth = req.query.coolerWidth;
    var videocardWidth = req.query.videocardWidth;
    var cases;
    try {
      cases = await caseModel.find(value && videocardWidth && coolerWidth ? 
        {
          value: value, 
          videocardMaxWidth: { $gt: videocardWidth }, 
          coolerMaxWidth: { $gt: coolerWidth }
        } :
        value && videocardWidth ? {
          value: value, 
          videocardMaxWidth: { $gt: videocardWidth }, 
        } :
        value && coolerWidth ? {
          value: value, 
          coolerMaxWidth: { $gt: coolerWidth }, 
        }
         : value ? {value: value}  
        : {}
      );
      res.json({ status: 'success', message: 'Cases get successful', data: { cases: cases } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};