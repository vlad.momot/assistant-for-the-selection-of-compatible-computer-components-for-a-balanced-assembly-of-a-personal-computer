const powerModel = require('../models/powers');

module.exports = {
  create: async function (req, res, next) {
    var newPower;
    try {
      newPower = await powerModel.create(req.body);
      res.json({ status: 'success', message: 'Power created successful', data: newPower });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedPower = req.body;
    delete updatedPower._id;

    try {
      updatedPower = await powerModel.findByIdAndUpdate(_id, updatedPower, { new: true });
      res.json({ status: 'success', message: 'Power updated successful', data: updatedPower });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var powers;
    try {
      powers = await powerModel.find(value ? {value: value} : {});
      res.json({ status: 'success', message: 'Powers get successful', data: { powers: powers } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};