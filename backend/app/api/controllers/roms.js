const romModel = require('../models/roms');

module.exports = {
  create: async function (req, res, next) {
    var newRom;
    try {
      newRom = await romModel.create(req.body);
      res.json({ status: 'success', message: 'Rom created successful', data: newRom });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedRom = req.body;
    delete updatedRom._id;

    try {
      updatedRom = await romModel.findByIdAndUpdate(_id, updatedRom, { new: true });
      res.json({ status: 'success', message: 'Rom updated successful', data: updatedRom });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var roms;
    try {
      roms = await romModel.find(value ? {value: value} : {});
      res.json({ status: 'success', message: 'Roms get successful', data: { roms: roms } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};