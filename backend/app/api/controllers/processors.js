const processorModel = require('../models/processors');

module.exports = {
  create: async function (req, res, next) {
    var newCPU;
    try {
      newCPU = await processorModel.create(req.body);
      res.json({ status: 'success', message: 'Processor created successful', data: newCPU });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedCPU = req.body;
    delete updatedCPU._id;

    try {
      updatedCPU = await processorModel.findByIdAndUpdate(_id, updatedCPU, { new: true });
      res.json({ status: 'success', message: 'Processor updated successful', data: updatedCPU });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var processorsLabels = req.query.processorsLabels;
    var processors;
    try {
      processors = await processorModel.find(processorsLabels && value ? {label: { $in: processorsLabels.split(',') }, value: value} : value ? {value: value} : {});
      res.json({ status: 'success', message: 'Processors get successful', data: { processors: processors } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};
