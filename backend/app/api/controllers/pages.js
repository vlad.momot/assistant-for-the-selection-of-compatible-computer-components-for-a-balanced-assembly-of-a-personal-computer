const axios = require('axios');

module.exports = {
    get: async function (req, res, next) {  
        var id = req.query.id;
        try {   
           const response = await axios.get(`${process.env.rozetka_url}get-main?front-type=xl&country=UA&lang=ua&goodsId=${id}`);
           res.json({ status: "success", message: `get data for ${id} success`, data: response.data.data });
        }
        catch (err) {
            res.json({ status: "error", message: err, data: null });
        }
    },
    getCharacteristic: async function (req, res, next) {  
        var id = req.query.id;
        try {   
           const response = await axios.get(`${process.env.rozetka_url}get-characteristic?front-type=xl&country=UA&lang=ua&goodsId=${id}`);
           res.json({ status: "success", message: `get data for ${id} success`, data: response.data.data });
        }
        catch (err) {
            res.json({ status: "error", message: err, data: null });
        }
    },
}