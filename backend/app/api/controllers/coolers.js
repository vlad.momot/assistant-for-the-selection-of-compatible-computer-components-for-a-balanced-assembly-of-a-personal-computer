const coolerModel = require('../models/coolers');

module.exports = {
  create: async function (req, res, next) {
    var newCooler;
    try {
      newCooler = await coolerModel.create(req.body);
      res.json({ status: 'success', message: 'Cooler created successful', data: newCooler });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedCooler = req.body;
    delete updatedCooler._id;

    try {
      updatedCooler = await coolerModel.findByIdAndUpdate(_id, updatedCooler, { new: true });
      res.json({ status: 'success', message: 'Cooler updated successful', data: updatedCooler });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var socket = req.query.socket;
    var TDP = req.query.TDP
    var coolers;
    try {
      coolers = await coolerModel.find(
        value && socket && TDP ? {value: value, sockets: { $elemMatch: { $eq: socket }}, TDP: { $gt: TDP }} :
        value && socket ? {value: value, sockets: { $elemMatch: { $eq: socket } }} :
        value ? {value: value} : {}
      );
      res.json({ status: 'success', message: 'Coolers get successful', data: { coolers: coolers } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};