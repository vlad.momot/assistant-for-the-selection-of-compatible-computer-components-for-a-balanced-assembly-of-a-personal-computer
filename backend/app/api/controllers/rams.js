const ramModel = require('../models/rams');

module.exports = {
  create: async function (req, res, next) {
    var newRam;
    try {
      newRam = await ramModel.create(req.body);
      res.json({ status: 'success', message: 'Ram created successful', data: newRam });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedRam = req.body;
    delete updatedRam._id;

    try {
      updatedRam = await ramModel.findByIdAndUpdate(_id, updatedRam, { new: true });
      res.json({ status: 'success', message: 'Ram updated successful', data: updatedRam });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var memoryType = req.query.memoryType;
    var rams;
    try {
      rams = await ramModel.find(
        value && memoryType ? { value: value, memoryType: memoryType} :
        value ? { value: value } : {});
      res.json({ status: 'success', message: 'Rams get successful', data: { rams: rams } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};