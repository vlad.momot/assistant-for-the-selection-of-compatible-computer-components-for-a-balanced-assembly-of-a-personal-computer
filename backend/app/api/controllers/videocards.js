const videocardModel = require('../models/videocards');

module.exports = {
  create: async function (req, res, next) {
    var newVideocard;
    try {
      newVideocard = await videocardModel.create(req.body);
      res.json({ status: 'success', message: 'Videocard created successful', data: newVideocard });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedVideocard = req.body;
    delete updatedVideocard._id;

    try {
      updatedVideocard = await videocardModel.findByIdAndUpdate(_id, updatedVideocard, { new: true });
      res.json({ status: 'success', message: 'Videocard updated successful', data: updatedVideocard });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var value = req.query.value;
    var videocards;
    try {
      videocards = await videocardModel.find(value ? {value: value} : {});
      res.json({ status: 'success', message: 'Videocards get successful', data: { videocards: videocards } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};