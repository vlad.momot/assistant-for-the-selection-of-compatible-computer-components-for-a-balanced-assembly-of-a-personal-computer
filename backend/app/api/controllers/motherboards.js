const motherboardModel = require('../models/motherboards');

module.exports = {
  create: async function (req, res, next) {
    var newMotherboard;
    try {
      newMotherboard = await motherboardModel.create(req.body);
      res.json({ status: 'success', message: 'Motherboard created successful', data: newMotherboard });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  update: async function (req, res, next) {
    var _id = req.body._id,
    updatedMotherboard = req.body;
    delete updatedMotherboard._id;

    try {
      updatedMotherboard = await motherboardModel.findByIdAndUpdate(_id, updatedMotherboard, { new: true });
      res.json({ status: 'success', message: 'Motherboard updated successful', data: updatedMotherboard });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },

  get: async function (req, res, next) {
    var socket = req.query.socket;
    var chipsets  = req.query.chipsets;
    var value = req.query.value;
    var motherboards;
    try {
      motherboards = await motherboardModel.find(
        chipsets && socket ? 
        {value: value, socket: socket, chipset: { $in: chipsets.split(',') }} 
        : socket ? 
        {value: value, socket: socket} 
        : {value: value}
      );
      res.json({ status: 'success', message: 'Motherboards get successful', data: { motherboards: motherboards } });
    } catch (err) {
      res.json({ status: 'error', message: err, data: null });
    }
  },
};