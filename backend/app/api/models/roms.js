const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RomSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    readSpeed: {
      type: String,
      required: false,
    },
    writeSpeed: {
      type: String,
      required: false,
    },
    speed: {
      type: String,
      required: false,
    },
    spindleRotationSpeed: {
      type: String,
      required: false,
    },
    isSSD: {
      type: Boolean,
      required: true,
    },
    isM2: {
      type: Boolean,
      required: false
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('ROM', RomSchema);