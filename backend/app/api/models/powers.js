const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PowerSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('Power', PowerSchema);