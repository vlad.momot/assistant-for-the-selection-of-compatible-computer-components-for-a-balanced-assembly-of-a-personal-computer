const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CaseSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    coolers: {
      type: Number,
      required: true,
    },
    videocardMaxWidth: {
      type: Number,
      required: true,
    },
    coolerMaxWidth: {
      type: Number,
      required: true,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('Case', CaseSchema);