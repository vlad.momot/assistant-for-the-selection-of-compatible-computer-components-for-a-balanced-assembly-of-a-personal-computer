const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RamSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    memoryType: {
      type: String,
      required: true,
    },
    memoryFrequency: {
      type: String,
      required: true,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('RAM', RamSchema);