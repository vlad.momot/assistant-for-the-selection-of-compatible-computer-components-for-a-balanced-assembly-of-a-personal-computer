const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const CoollerSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    sockets: {
      type: Array,
      required: true,
    },
    TDP: {
      type: Number,
      required: true,
    },
    width: {
      type: Number,
      required: false,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('Cooller', CoollerSchema);