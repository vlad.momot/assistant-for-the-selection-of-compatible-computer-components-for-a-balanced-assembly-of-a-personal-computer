const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const VideocardSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    power: {
      type: Number,
      required: true,
    },
    memoryBusWidth: {
      type: String,
      required: true,
    },
    memoryFrequency: {
      type: String,
      required: true,
    },
    coreFrequency: {
      type: String,
      required: true,
    },
    width: {
      type: Number,
      required: false,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('Videocard', VideocardSchema);