const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ProcessorSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    socket: {
      type: String,
      required: true,
    },
    cores: {
      type: Number,
      required: true,
    },
    threads: {
      type: Number,
      required: true,
    },   
    coreArchitecture: {
      type: String,
      required: false,
    },
    maxFrequency: {
      type: Number,
      required: true,
    },
    integratedGraphics: {
      type: String,
      required: false,
    },
    power: {
      type: Number,
      required: true,
    },
    L3cache: {
      type: String,
      required: true,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('Processor', ProcessorSchema);