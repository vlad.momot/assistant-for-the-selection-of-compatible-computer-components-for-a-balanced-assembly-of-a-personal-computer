const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const MotherboardSchema = new Schema({
    label: {
      type: String,
      required: true,
    },
    value: {
      type: String,
      required: true,
    },
    chipset: {
      type: String,
      required: true,
    },
    socket: {
      type: String,
      required: true,
    },
    rozetkaId: {
      type: String,
      required: true,
    }
});

module.exports = mongoose.model('Motherboard', MotherboardSchema);