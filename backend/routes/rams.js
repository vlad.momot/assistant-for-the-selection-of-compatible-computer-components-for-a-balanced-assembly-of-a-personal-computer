const express = require('express');
const router = express.Router();
const ramController = require('../app/api/controllers/rams');

router.post('/create', ramController.create);
router.post('/update', ramController.update);
router.get('/get', ramController.get);

module.exports = router;