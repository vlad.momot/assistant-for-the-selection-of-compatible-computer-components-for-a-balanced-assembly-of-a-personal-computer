const express = require('express');
const router = express.Router();
const powerController = require('../app/api/controllers/powers');

router.post('/create', powerController.create);
router.post('/update', powerController.update);
router.get('/get', powerController.get);

module.exports = router;