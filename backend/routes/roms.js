const express = require('express');
const router = express.Router();
const romController = require('../app/api/controllers/roms');

router.post('/create', romController.create);
router.post('/update', romController.update);
router.get('/get', romController.get);

module.exports = router;