const express = require('express');
const router = express.Router();
const pagesController = require('../app/api/controllers/pages');

router.get('/get', pagesController.get);
router.get('/getCharacteristic', pagesController.getCharacteristic);

module.exports = router;