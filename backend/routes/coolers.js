const express = require('express');
const router = express.Router();
const coolerController = require('../app/api/controllers/coolers');

router.post('/create', coolerController.create);
router.post('/update', coolerController.update);
router.get('/get', coolerController.get);

module.exports = router;