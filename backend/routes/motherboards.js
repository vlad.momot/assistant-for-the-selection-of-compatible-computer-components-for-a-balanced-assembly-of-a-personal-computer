const express = require('express');
const router = express.Router();
const motherboardController = require('../app/api/controllers/motherboards');

router.post('/create', motherboardController.create);
router.post('/update', motherboardController.update);
router.get('/get', motherboardController.get);

module.exports = router;