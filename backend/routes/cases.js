const express = require('express');
const router = express.Router();
const caseController = require('../app/api/controllers/cases');

router.post('/create', caseController.create);
router.post('/update', caseController.update);
router.get('/get', caseController.get);

module.exports = router;