const express = require('express');
const router = express.Router();
const processorController = require('../app/api/controllers/processors');

router.post('/create', processorController.create);
router.post('/update', processorController.update);
router.get('/get', processorController.get);

module.exports = router;