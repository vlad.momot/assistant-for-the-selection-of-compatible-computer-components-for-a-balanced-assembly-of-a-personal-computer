const express = require('express');
const router = express.Router();
const videocardController = require('../app/api/controllers/videocards');

router.post('/create', videocardController.create);
router.post('/update', videocardController.update);
router.get('/get', videocardController.get);

module.exports = router;