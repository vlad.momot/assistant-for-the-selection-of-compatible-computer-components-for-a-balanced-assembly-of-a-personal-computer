const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config();
const mongoDB = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.hyuxcsy.mongodb.net/configurator`;

mongoose.connect(mongoDB, {
  useCreateIndex: true,
  useNewUrlParser: true,
  useFindAndModify: false,  
  useUnifiedTopology: true
});

mongoose.Promise = global.Promise;

module.exports = mongoose;
