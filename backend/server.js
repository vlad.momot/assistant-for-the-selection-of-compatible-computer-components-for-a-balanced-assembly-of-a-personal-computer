const express = require('express');
const logger = require('morgan');
const compression = require('compression');
var path = require('path');

const pages = require('./routes/pages');
const processors = require('./routes/processors');
const videocards = require('./routes/videocards');
const motherboards = require('./routes/motherboards');
const rams = require('./routes/rams');
const roms = require('./routes/roms');
const powers = require('./routes/powers');
const coolers = require('./routes/coolers');
const cases = require('./routes/cases');

const bodyParser = require('body-parser');
const mongoose = require('./config/database'); //database configuration
const cors = require('cors');

const dotenv = require('dotenv');
dotenv.config();

const app = express();
app.use(compression());

mongoose.connection.on(
  'error',
  console.error.bind(console, 'MongoDB connection error:')
);

app.use(logger('dev'));
app.use(bodyParser.json({ limit: '100mb' }));
app.use(
  bodyParser.urlencoded({
    limit: '100mb',
    extended: false,
  })
);

app.use(express.json());
app.use(cors());
app.use(express.static(path.join(__dirname, 'dist')));

app.use(/^((?!(api|history)).)*/, (req, res) => {
  res.sendFile(path.join(__dirname, '/dist/index.html'));
});

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

global.appRoot = path.resolve(__dirname);

// public routes
app.use('/api/pages', pages);
app.use('/api/processors', processors);
app.use('/api/videocards', videocards);
app.use('/api/motherboards', motherboards);
app.use('/api/rams', rams);
app.use('/api/roms', roms);
app.use('/api/powers', powers);
app.use('/api/coolers', coolers);
app.use('/api/cases', cases);

app.get('/favicon.ico', function (req, res) {
  res.sendStatus(204);
});

app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  if (err.status === 404) res.json({ status: 404, message: null, data: null });
  else res.json({ status: 500, message: null, data: null });
});

var http = require('http');
http.createServer(app).listen(process.env.PORT || 3000);
console.log(`Server has been started on port ${process.env.PORT}...`)