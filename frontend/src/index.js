import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router } from "react-router-dom";
import { Provider } from 'react-redux';
import { store } from './Stores/index';

import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));

function AppInit() {
  return (
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>
  );
}

root.render(<AppInit />);
