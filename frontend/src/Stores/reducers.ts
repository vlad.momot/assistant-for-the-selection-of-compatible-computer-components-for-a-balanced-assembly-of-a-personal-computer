import { combineReducers } from 'redux';
import mainReducer from '../Globals/MainPage/reducer';
import appReducer from '../Globals/App/reducer';

export const whiteListReducers = {

};

export const blackListReducers = {
  app: appReducer,
  main: mainReducer,
};



const rootReducer = combineReducers({
  ...blackListReducers,
  ...whiteListReducers,
});


export default rootReducer;
