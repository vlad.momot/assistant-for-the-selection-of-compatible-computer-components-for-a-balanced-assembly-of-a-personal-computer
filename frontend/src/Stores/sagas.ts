import { all } from 'redux-saga/effects';
import WatcherMainSaga from '../Globals/MainPage/sagas';

export default function* rootSaga() {
  yield all([
    WatcherMainSaga(),
  ]);
}
