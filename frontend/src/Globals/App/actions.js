import * as appConstants from './constants';

export const appActions = {
  openLoader: () =>
    ({
      type: appConstants.OPEN_LOADER,
    }),
  closeLoader: () =>
    ({
      type: appConstants.CLOSE_LOADER,
    }),
};
