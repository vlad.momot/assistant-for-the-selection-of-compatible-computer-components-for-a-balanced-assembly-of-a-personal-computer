import * as appConstants from './constants';

const initialState = {
  loading: false,
};

const appReducer = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case appConstants.OPEN_LOADER:
      return {
        ...state,
        loading: true,
      };
    case appConstants.CLOSE_LOADER:
      return {
        ...state,
        loading: false,
    };
    default:
      return state;
  }
};

export default appReducer;
