import * as mainConstants from './constants';

const initialState = {
  processorData: {},
  coolerData: {},
  motherboardData: {},
  ramData: {},
  hddData: {},
  ssd1Data: {},
  ssd2Data: {},
  videocardData: {},
  powerData: {},
  caseData: {},
  pageData: [],
  processors: [],
  coolers: [],
  motherboards: [],
  rams: [],
  HDDs: [],
  SSDs: [],
  SSD2s: [],
  videocards: [],
  powers: [],
  cases: [],
};

const mainReducer = (
  state = initialState,
  action,
) => {
  switch (action.type) {
    case mainConstants.GET_PAGE_SUCCESS:
        switch (action.payload.pageType) {
          case 'processor':
            return {
              ...state,
              processorData: action.payload
            };
          case 'cooler':
            return {
              ...state,
              coolerData: action.payload
            };
          case 'motherboard':
            return {
              ...state,
              motherboardData: action.payload
            };
          case 'ram':
            return {
              ...state,
              ramData: action.payload
            };
          case 'hdd':
            return {
              ...state,
              hddData: action.payload
            };
          case 'ssd1':
            return {
              ...state,
              ssd1Data: action.payload
            };
          case 'ssd2':
            return {
              ...state,
              ssd2Data: action.payload
            };
          case 'videocard':
            return {
              ...state,
              videocardData: action.payload
            };
          case 'power':
            return {
              ...state,
              powerData: action.payload
            };
          case 'case':
            return {
              ...state,
              caseData: action.payload
            };
          default:
            return {
              ...state,
              pageData: action.payload
            };
        }  
    case mainConstants.GET_PROCESSORS_SUCCESS:
      return {
        ...state,
        processors: action.payload,
      };    
    case mainConstants.GET_COOLERS_SUCCESS:
      return {
        ...state,
        coolers: action.payload,
      };    
    case mainConstants.GET_MOTHERBOARDS_SUCCESS:
      return {
        ...state,
        motherboards: action.payload,
      };    
    case mainConstants.GET_RAMS_SUCCESS:
      return {
        ...state,
        rams: action.payload,
      };    
    case mainConstants.GET_VIDEOCARDS_SUCCESS:
      return {
        ...state,
        videocards: action.payload,
      }; 
    case mainConstants.GET_CASES_SUCCESS:
      return {
        ...state,
        cases: action.payload,
      };  
    case mainConstants.GET_HDDS_SUCCESS:
      return {
        ...state,
        HDDs: action.payload,
      };         
    case mainConstants.GET_SSDS_SUCCESS:
      return {
        ...state,
        SSDs: action.payload,
      }; 
    case mainConstants.GET_SSDS2_SUCCESS:
      return {
        ...state,
        SSD2s: action.payload,
      };       
    case mainConstants.GET_POWERS_SUCCESS:
      return {
        ...state,
        powers: action.payload,
      };    
    default:
      return state;
  }
};

export default  mainReducer;
