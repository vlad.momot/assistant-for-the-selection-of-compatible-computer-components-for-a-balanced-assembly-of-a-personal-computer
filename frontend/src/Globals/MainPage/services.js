import axios from 'axios';

export const mainApi = {
  getPage: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}pages/get?id=${payload.id}`,
    );
    return resp.data;
  },
  getCharacteristic: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}pages/getCharacteristic?id=${payload.id}`,
    );
    return resp.data;
  },
  getProcessors: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}processors/get?value=${payload?.value || ''}&&processorsLabels=${payload?.processorsLabels || ''}`,
    );
    return resp.data;
  },
  getCoolers: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}coolers/get?value=${payload?.value || ''}&socket=${payload?.socket || ''}&TDP=${payload?.tdp || ''}`,
    );
    return resp.data;
  },
  getMotherboards: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}motherboards/get?value=${payload?.value || ''}&socket=${payload?.socket || ''}&chipsets=${payload?.chipsets || ''}`,
    );
    return resp.data;
  },
  getRams: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}rams/get?value=${payload?.value || ''}&memoryType=${payload?.memoryType || ''}`,
    );
    return resp.data;
  },
  getRoms: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}roms/get?isSSD=${payload?.isSSD || false}&value=${payload?.value || ''}`,
    );
    return resp.data;
  },
  getVideocards: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}videocards/get?value=${payload?.value || ''}`,
    );
    return resp.data;
  },
  getPowers: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}powers/get?value=${payload?.value || ''}`,
    );
    return resp.data;
  },
  getCases: async (payload) => {
    const resp = await axios.get(
      `${process.env.REACT_APP_API_URL}cases/get?value=${payload?.value || ''}&videocardWidth=${payload?.videocardWidth || ''}&coolerWidth=${payload?.coolerWidth || ''}`,
    );
    return resp.data;
  },
  create: async (payload) => {
    const resp = await axios.post(
      `${process.env.REACT_APP_API_URL}accessoriesName/create`,
      payload,
    );
    return resp.data;
  }
};