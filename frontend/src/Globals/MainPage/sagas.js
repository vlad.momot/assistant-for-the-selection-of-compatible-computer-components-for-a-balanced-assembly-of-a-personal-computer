
import { call, takeLatest, put } from 'redux-saga/effects';

import { mainApi } from './services';
import { mainActions } from './actions';
import { appActions } from '../App/actions';
import * as mainConstants from './constants';

export function* getPage({payload}) {
  try {
    yield put(appActions.openLoader());
    const result = yield call(mainApi.getPage, payload);
    const resultCharacteristic = yield call(mainApi.getCharacteristic, payload);
    yield put(mainActions.getPageSuccess({...result.data, characteristic: resultCharacteristic.data, data: payload.data, pageType: payload.pageType}));
    yield put(appActions.closeLoader());
  } catch (e) {
    yield put(appActions.closeLoader());
  }
}

export function* getProcessors({payload}) {
  try {
    const result = yield call(mainApi.getProcessors, payload);
    yield put(mainActions.getProcessorsSuccess(result.data.processors));
  } catch (e) {
    throw(e);
  }
}

export function* getCoolers({payload}) {
  try {
    const result = yield call(mainApi.getCoolers, payload);
    yield put(mainActions.getCoolersSuccess(result.data.coolers));
  } catch (e) {
    throw(e);
  }
}

export function* getMotherboards({payload}) {
  try {
    const result = yield call(mainApi.getMotherboards, payload);
    yield put(mainActions.getMotherboardsSuccess(result.data.motherboards));
  } catch (e) {
    throw(e);
  }
}

export function* getRams({payload}) {
  try {
    const result = yield call(mainApi.getRams, payload);
    yield put(mainActions.getRamsSuccess(result.data.rams));
  } catch (e) {
    throw(e);
  }
}

export function* getVideocards({payload}) {
  try {
    const result = yield call(mainApi.getVideocards, payload);
    yield put(mainActions.getVideocardsSuccess(result.data.videocards));
  } catch (e) {
    throw(e);
  }
}

export function* getPowers({payload}) {
  try {
    const result = yield call(mainApi.getPowers, payload);
    yield put(mainActions.getPowersSuccess(result.data.powers));
  } catch (e) {
    throw(e);
  }
}

export function* getHDDs({payload}) {
  try {
    const result = yield call(mainApi.getRoms, payload);
    yield put(mainActions.getHDDsSuccess(result.data.roms));
  } catch (e) {
    throw(e);
  }
}

export function* getSSDs({payload}) {
  try {
    const result = yield call(mainApi.getRoms, payload);
    yield put(mainActions.getSSDsSuccess(result.data.roms));
  } catch (e) {
    throw(e);
  }
}

export function* getSSD2s({payload}) {
  try {
    const result = yield call(mainApi.getRoms, payload);
    yield put(mainActions.getSSD2sSuccess(result.data.roms));
  } catch (e) {
    throw(e);
  }
}

export function* getCases({payload}) {
  try {
    const result = yield call(mainApi.getCases, payload);
    yield put(mainActions.getCasesSuccess(result.data.cases));
  } catch (e) {
    throw(e);
  }
}

export function* create({payload}) {
  try {
    yield put(appActions.openLoader());
    yield call(mainApi.create, payload);
    yield put(appActions.closeLoader());
  } catch (e) {
    yield put(appActions.closeLoader());
  }
}


export default function* WatcherSaga() {
  yield takeLatest(mainConstants.GET_PAGE, getPage);
  yield takeLatest(mainConstants.GET_PROCESSORS, getProcessors);
  yield takeLatest(mainConstants.GET_COOLERS, getCoolers);
  yield takeLatest(mainConstants.GET_MOTHERBOARDS, getMotherboards);
  yield takeLatest(mainConstants.GET_RAMS, getRams);
  yield takeLatest(mainConstants.GET_HDDS, getHDDs);
  yield takeLatest(mainConstants.GET_SSDS, getSSDs);
  yield takeLatest(mainConstants.GET_SSDS2, getSSD2s);
  yield takeLatest(mainConstants.GET_POWERS, getPowers);
  yield takeLatest(mainConstants.GET_VIDEOCARDS, getVideocards);
  yield takeLatest(mainConstants.GET_CASES, getCases);
  yield takeLatest(mainConstants.CREATE, create);
}
