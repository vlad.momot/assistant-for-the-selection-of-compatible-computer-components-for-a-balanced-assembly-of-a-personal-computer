import * as mainConstants from './constants';

export const mainActions = {
  getPage: (payload) =>
  ({
    type: mainConstants.GET_PAGE,
    payload
  }),
  getPageSuccess: (payload) =>
  ({
    type: mainConstants.GET_PAGE_SUCCESS,
    payload
  }),
  getProcessors: (payload) =>
  ({
    type: mainConstants.GET_PROCESSORS,
    payload
  }),
  getProcessorsSuccess: (payload) =>
  ({
    type: mainConstants.GET_PROCESSORS_SUCCESS,
    payload
  }),
  getCoolers: (payload) =>
  ({
    type: mainConstants.GET_COOLERS,
    payload
  }),
  getCoolersSuccess: (payload) =>
  ({
    type: mainConstants.GET_COOLERS_SUCCESS,
    payload
  }),
  getMotherboards: (payload) =>
  ({
    type: mainConstants.GET_MOTHERBOARDS,
    payload
  }),
  getMotherboardsSuccess: (payload) =>
  ({
    type: mainConstants.GET_MOTHERBOARDS_SUCCESS,
    payload
  }),
  getRams: (payload) =>
  ({
    type: mainConstants.GET_RAMS,
    payload
  }),
  getRamsSuccess: (payload) =>
  ({
    type: mainConstants.GET_RAMS_SUCCESS,
    payload
  }),
  getHDDs: (payload) =>
  ({
    type: mainConstants.GET_HDDS,
    payload
  }),
  getHDDsSuccess: (payload) =>
  ({
    type: mainConstants.GET_HDDS_SUCCESS,
    payload
  }),
  getSSDs: (payload) =>
  ({
    type: mainConstants.GET_SSDS,
    payload
  }),
  getSSDsSuccess: (payload) =>
  ({
    type: mainConstants.GET_SSDS_SUCCESS,
    payload
  }),
  getSSD2s: (payload) =>
  ({
    type: mainConstants.GET_SSDS2,
    payload
  }),
  getSSD2sSuccess: (payload) =>
  ({
    type: mainConstants.GET_SSDS2_SUCCESS,
    payload
  }),
  getVideocards: (payload) =>
  ({
    type: mainConstants.GET_VIDEOCARDS,
    payload
  }),
  getVideocardsSuccess: (payload) =>
  ({
    type: mainConstants.GET_VIDEOCARDS_SUCCESS,
    payload
  }),
  getPowers: (payload) =>
  ({
    type: mainConstants.GET_POWERS,
    payload
  }),
  getPowersSuccess: (payload) =>
  ({
    type: mainConstants.GET_POWERS_SUCCESS,
    payload
  }),
  getCases: (payload) =>
  ({
    type: mainConstants.GET_CASES,
    payload
  }),
  getCasesSuccess: (payload) =>
  ({
    type: mainConstants.GET_CASES_SUCCESS,
    payload
  }),
  create: (payload) =>
  ({
    type: mainConstants.CREATE,
    payload
  }),
};
