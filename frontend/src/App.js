import React from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor } from '../src/Stores/index';
import { Routes, Route} from "react-router-dom";
import MainPage from './Pages/MainPage';
import CartPage from './Pages/CartPage';

import SideMenu from './Components/Sidebar/SideMenu';
import CustomLoader from './Components/CustomLoader';

function App() {
  return (
    <div style={{display: "flex", height: "100vh", overflow: 'hidden'}}>
      <PersistGate loading={null} persistor={persistor}>
        <SideMenu />
        <CustomLoader />
        <Routes>
          <Route exact path={'/'} element={<MainPage/>} />
          <Route exact path={'/cart'} element={<CartPage/>} />
        </Routes>
      </PersistGate>
    </div>
  );
}
export default App;
