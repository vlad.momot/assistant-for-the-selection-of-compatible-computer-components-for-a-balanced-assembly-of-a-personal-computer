import React from 'react';
import './styles.scss'

const LineDivider = ({ style = {} }) => {
  return (
    <div className="lineDivider" style={{...style}}>
    </div>
  );
}

export default LineDivider;