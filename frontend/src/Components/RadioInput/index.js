import './styles.scss';

const RadioInput = ({ name, label, value, isChecked, handleChange, id }) => {
    const handleRadioClick = e => {
      const { value } = e.currentTarget;
      handleChange(value);
    };
  
    return (
      <div className="customRadio" style={{marginTop: '20px'}}>
        <input
          type="checkbox"
          className="custom-radio"
          name={name}
          id={id}
          value={value}
          checked={isChecked === value}
          onClick={handleRadioClick}
          onChange={() => {}}
        />
        <label htmlFor={id}>
          <div className="labelText">{label}</div>
        </label>
      </div>
    );
  };

export default RadioInput;