import React from 'react';
import Table from 'rc-table';
import './style.scss';

const CustomTable = ({ columns, data, scroll = { y: 800, x: 450 }, onClick }) => {
	const onRowClick = (state) => {
		return {
			onClick: e => {
				console.log('A Td Element was clicked!', state)
				onClick(state)
			}
		}
	}
	return (
		<Table
			columns={columns}
			data={data}
			scroll={scroll}
			onRow={onRowClick}
			tableLayout="auto"
		/>
	)
}
export default CustomTable;