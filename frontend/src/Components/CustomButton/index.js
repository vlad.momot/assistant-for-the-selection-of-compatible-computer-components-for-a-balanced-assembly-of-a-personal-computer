import React from 'react';
import cn from 'classnames';
import './styles.scss';

const CustomButton = ({ title, icon, iconRight, type = 'primary', style = {}, onClick = () => {} }) => {
  return (
    <div
      className={
        cn({
          'customBtn': true,
          'primaryBtn': type === 'primary',
          'blueBtn': type === 'blue',
        })}
      style={{ ...style }}
      onClick={onClick}
    >
      {iconRight ? <img src={iconRight} alt=""/> : <></> }
      {title ? <div className={
        cn({
          'customBtnTitle': true,
          'primaryTitle': type === 'primary',
          'blueTitle': type === 'blue',
        })}>
        {title}
      </div> : <></>}
      <img src={icon} alt=""/>
    </div>
  );
}

export default CustomButton;