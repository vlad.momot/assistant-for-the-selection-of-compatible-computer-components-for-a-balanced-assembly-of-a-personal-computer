import React, { useEffect } from 'react';
import Modal from 'react-modal';
import { CSSTransition } from 'react-transition-group';
import CustomButton from '../CustomButton';
import LineDivider from '../LineDivider';
import arrow_left from '../../Assets/Images/arrow-left-dark.svg';
import './styles.scss';

const CustomModal = ({children, modalIsOpen, height, setIsOpen, empty = false, style = {}, headerStyle = {} }) => {
  const customStyles = {
    content: {
      position: 'unset',
      padding: '30px',
      border: 'none',
      boxShadow: '0px 4px 51px rgba(0, 0, 0, 0.25)',
      borderRadius: '0px',
      width: '80%',
      maxWidth: '450px',
      height: height
    },
    overlay: {
      display: 'flex',
      position: 'fixed',
      alignItems: 'center',
      justifyContent: 'center',
    }
  };

  useEffect(() => {
    if (modalIsOpen) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'unset';
    }
    return () => {
      document.body.style.overflow = 'unset';
    }
  }, [modalIsOpen]);

  function closeModal () {
    setIsOpen(false);
  }

  return (
    <CSSTransition
      in={modalIsOpen}
      timeout={300}
      classNames={{
        appear: 'my-appear',
        appearActive: 'my-active-appear',
        appearDone: 'my-done-appear',
        enter: 'my-enter',
        enterActive: 'my-active-enter',
        enterDone: 'my-done-enter',
        exit: 'my-exit',
        exitActive: 'my-active-exit',
        exitDone: 'my-done-exit',
      }}
      unmountOnExit
    >
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        style={{ ...customStyles, content: { ...style, ...customStyles.content } }}
        contentLabel="Example Modal"
        closeTimeoutMS={300}
      >
        {!empty ? 
          <div style={headerStyle}>
            <CustomButton
              icon={arrow_left}
              title={'Закрити'}
              onClick={closeModal}
              style={{ height: '30px' }}
              type="blue"
            />
            <LineDivider style={{ marginTop: '15px', marginBottom: '15px' }} />
          </div>
        : <></>}
        {children}
      </Modal>
    </CSSTransition>
  );
}

Modal.setAppElement('#root')

export default CustomModal;