import React, { useEffect } from 'react';
import toast from 'react-hot-toast';

import './styles.scss';

import Close from '../../Assets/Images/toast/close.svg';
import WarningIcon from '../../Assets/Images/toast/warning.svg';
import SuccessIcon from '../../Assets/Images/toast/success.svg';
import { useNavigate } from 'react-router-dom';
const CustomToast = ({theme = 'error', title, id, cart=false }) => {
    const navigate = useNavigate();
    
    useEffect(() => {
        setTimeout(() => {
            toast.remove(id);
        }, 5000);
    }, [id]);

    const onClose = () => {
        toast.remove(id);
    }

    const toCart = () => {
        navigate('/cart')
    }

	return (
		<div className="toastWrapper" style={{backgroundColor: theme === 'success' ? '#7ED321' : '#dc5b5b'}}>
            <div className="toastContent">
            <img style={{paddingRight: '20px' }} src={theme === 'success' ? SuccessIcon : WarningIcon} alt="" />
			<div className="toastText">{title}</div>
            {cart ?
            <div className="toastButton" onClick={toCart}>
                <div className="toastText">До корзини</div>
            </div>
            : <></>
            }
			<div className="toastButton" onClick={onClose}>
                <img src={Close} alt="" />
                <div className="toastText">Закрити</div>
            </div>
            </div>
            <div className="toastLine" style={{backgroundColor: theme === 'success' ? '#6EBA1B' : '#942C2C'}}></div>
		</div>
	);
};

export default CustomToast;