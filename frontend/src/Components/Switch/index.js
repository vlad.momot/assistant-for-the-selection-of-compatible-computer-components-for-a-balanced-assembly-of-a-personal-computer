import React from 'react';
import './styles.scss';

const Switch = ({ isOn, handleToggle, title, id, ...props }) => {
  return (
    <div className="customSwitch">
      <input
        checked={isOn}
        onChange={(e) => handleToggle(e.target.checked)}
        className="react-switch-checkbox"
        id={id}
        type="checkbox"
        {...props}
      />
      <label
        style={{ background: isOn && '#227093' }}
        className="react-switch-label"
        htmlFor={id}
      >
        <span
          className={`react-switch-button`}
        >
        </span>
      </label>
      <div className="customSwitchTitle">
        {title}
      </div>
    </div>
  );
};

export default Switch;