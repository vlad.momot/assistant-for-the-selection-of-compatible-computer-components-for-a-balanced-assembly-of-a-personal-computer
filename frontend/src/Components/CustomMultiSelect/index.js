import React, {useState, useEffect} from 'react';
import { MultiSelect } from "react-multi-select-component";
import './styles.scss';

const CustomMultiSelect = ({
  selected = [],
  setSelected,
  title,
  options = [],
  oneRequired = false,
  style,
  multi = false,
  placeholder = 'Не вибрано',
  titleStyle,
  disabled = false,
  className = "",
}) => {
  const [avaliableOptions, setAvaliableOptions] = useState(options);
  const [allOptions, setAllOptions] = useState(options);

  const onChange = (value) => {
    if (oneRequired && value?.length === 0) {
      return;
    }
    if (!multi && value?.length >= 1) {
      setSelected(value.slice(-1));
      return;
    }
    setSelected(value);
  }

  useEffect(() => {
    setAvaliableOptions(allOptions);
	}, [selected, allOptions]);

  useEffect(() => { 
    setAllOptions(options);
	}, [options]);

 
  const customValueSingleRenderer = (selected, _options) => {
    return selected?.length
      ? <>{selected.map(({ label }) =>
        <div className="customValueTextSingle" key={label}>{label}</div>
      )} </>
      : <div className="customValueTextSingle">{placeholder}</div>;
  };

  const customItemRenderer = ({ option, onClick, disabled, checked }) => {
    return (
      <div className={`customItem ${checked ? 'active' : ''}`} onClick={onClick} disabled={disabled}>
        {option.label}
      </div>
    )
  };

  return (
    <div className="selectContainer" style={style}>
      {title ? <div className="selectTitle" style={titleStyle}>
        {title}
      </div> : <></>}
      <MultiSelect
        options={avaliableOptions}
        value={selected}
        onChange={onChange}
        hasSelectAll={false}
        disableSearch={true}
        valueRenderer={customValueSingleRenderer}
        ItemRenderer={customItemRenderer}
        disabled={disabled}
        className={className}
      />
    </div>
  );
}

export default CustomMultiSelect;