import './styles.scss'

const InputWithIcon = ({
							 title,
						   placeholder,
						   icon,
							 value,
							 setValue,
						   visible = true,
						   shadow = false,
						   isFormInput = false,
							 center = false,
							 maxlength = 254,
							 register = () => { return {}},
							 style,
							 inputStyle,
						   error,
						   ...props
					   }) => {

	const customProps = !isFormInput ? {
		value,
		onChange: (e) => {
			setValue(e.target.value)
		}
	} : {
	};

	return (
		<div className={`inputWrapper ${!visible ? 'notVisible' : ''}  ${center ? 'centerAlignInput' : ''}`}>
			{title ? <div className="inputWithIconTitle">{title}</div> : <></>}
			<div className={`inputItem`} style={style}>
				{ icon ? 
					<img 
						src={icon}
						className={`inputIcon ${!visible ? 'notVisible' : ''}`}
						alt="input"
					/> : <></>
				}
				<input
					className={`inputWithIcon ${shadow ? 'inputShadow' : ''}`}
					placeholder={placeholder}
					{...customProps}
					{...register(props.special_label || props.label, { ...props.pattern })}
					{...props}
					style={{...inputStyle, border: error ? '1px solid red': 'none'}}
					maxLength={maxlength}
				/>
			</div>
			{error?.message ? <div className='requirmentMessage'>{error.message}</div> : <></>}
		</div>
	);
};

export default InputWithIcon;
