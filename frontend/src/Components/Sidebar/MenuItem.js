import React from 'react';
import { NavLink } from 'react-router-dom';

const MenuItem = (props) => {
    const { name, img, to, exact, inactive } = props;
    return (
        <>
            <NavLink exact={exact} to={to} className="menu-item" >
                <div className="menu-icon" data-tooltip={inactive ? null : name}>
                    <img src={img} alt="" />
                </div>
                <span className={`menu-text ${inactive ? 'inactive' : ''}`}>
                    {name}
                </span>
            </NavLink>
        </>
    )
};

export default MenuItem;