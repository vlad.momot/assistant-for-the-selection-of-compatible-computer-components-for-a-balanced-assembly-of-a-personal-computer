import React, { useState, useEffect } from 'react';
import MenuItem from './MenuItem';
import Logo from "../../Assets/Images/logo.png";
import MainPage from "../../Assets/Images/Administration.svg";
import Arrow from "../../Assets/Images/Arrow.svg";
import ArrowBack from "../../Assets/Images/ArrowBack.svg";
import Cart from "../../Assets/Images/shopping-cart.svg";
import './style.scss';

const menuItems = [
	{ name: 'Конфігуратор', img: MainPage, to: '/', exact: true },
	{ name: 'Корзина', img: Cart, to: '/cart', exact: true },
]

const SideMenu = () => {
	const [inactive, setInactive] = useState(false);
	const [windowWidth, setWindowWidth] = useState(window.innerWidth);

	useEffect(() => {
    function handleResize() {
      setWindowWidth(window.innerWidth);
    }

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

	return (
		<div style={{ position: 'sticky', height: '100vh', zIndex: 9 }}>
			<div className={`side-menu side-menu-additional ${inactive ? 'inactive' : ''}`} >
				<div>
					<div className="top-section">
						<div className="logo">
							<img width={65} src={Logo} alt="Logo" />
						</div>
					</div>
					<div className="main-menu">
						<ul>
							{
								menuItems.map((menuItem, index) => { 
									return (
											<MenuItem
												key={index}
												exact={menuItem.exact.toString()}
												name={menuItem.name}
												img={menuItem.img}
												to={menuItem.to}
												inactive={inactive}
											/>
									)
								})
								
							}
						</ul>
					</div>	
					{windowWidth > 500 ?
						<> 
							<div className={`divider ${inactive ? 'inactive' : ''}`}></div>
							<div className="toogle-menu-btn" onClick={() => setInactive(!inactive)}>
								<a href className="menu-item" data-tooltip={inactive ? null : 'Відкрити'}>
									<div className="menu-icon">
										{inactive ? <img src={Arrow} alt="toggle menu btn" /> : <img src={ArrowBack} alt="toggle menu btn" />}
									</div>
									<span className={`menu-text ${inactive ? 'inactive' : ''}`}>Закрити</span>
								</a>
							</div>
						</> : <></>
					}
				</div>
			</div>
		</div>
	)
}

export default SideMenu;
