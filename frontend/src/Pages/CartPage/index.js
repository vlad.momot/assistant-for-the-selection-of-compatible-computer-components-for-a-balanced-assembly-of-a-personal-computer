import React, { useEffect, useState } from 'react';
import CustomTable from '../../Components/CustomTable';
import CustomButton from '../../Components/CustomButton';
import emailjs from '@emailjs/browser';
import './styles.scss';
import LineDivider from '../../Components/LineDivider';
import CustomModal from '../../Components/CustomModal';
import EmailIcon from '../../Assets/Images/login/email.svg';
import InputWithIcon from '../../Components/InputWithIcon';
import CustomToast from '../../Components/CustomToast';
import toast, { Toaster } from 'react-hot-toast';
import { useForm } from "react-hook-form";
import Switch from '../../Components/Switch';

const CartPage = () => {
  const [cartData, setCartData] = useState();
  const [tableData, setTableData] = useState([]);
  const [totalCost, setTotalCost] = useState(0);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [emailSave, setEmailSave] = useState(JSON.parse(localStorage.getItem('emailSave')) || false);
  const { 
    register, 
    handleSubmit, 
    setValue, 
    clearErrors, 
    formState: { isValid, errors } 
  } = useForm({ mode: 'onChange' });

  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

	useEffect(() => {
    function handleResize() {
      setWindowWidth(window.innerWidth);
    }

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const columns = [
    {
      title: 'Тип',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: 'Назва',
      dataIndex: 'label',
      key: 'label',
    },
    {
      title: 'Ціна',
      dataIndex: 'price',
      key: 'price',
    },
  ];

  useEffect(() => {
    setCartData(JSON.parse(localStorage.getItem('cartData')));
    const cart_data = JSON.parse(localStorage.getItem('cartData'));
    const data = [];
    if (cart_data?.processor?.data) {
      data.push({ type: 'Процесор', label: cart_data.processor.data.label, price: `${cart_data.processor.price}₴`, href: cart_data.processor.href, key: '1' });
      data.push({ type: 'Охолодження', label: cart_data.cooler.data.label, price: `${cart_data.cooler.price}₴`, href: cart_data.cooler.href, key: '2' });
      data.push({ type: 'Материнська плата', label: cart_data.motherboard.data.label, price: `${cart_data.motherboard.price}₴`, href: cart_data.motherboard.href, key: '3' });
      data.push({ type: "Оперативна пам'ять", label: cart_data.ram.data.label, price: `${cart_data.ram.price}₴`, href: cart_data.ram.href, key: '4' });
    }
    if (cart_data?.videocard?.data) {
      data.push({ type: 'Відеокарта', label: cart_data.videocard.data.label, price: `${cart_data.videocard.price}₴`, href: cart_data.videocard.href, key: '5' });
    }
    if (cart_data?.power?.data) {
      data.push({ type: 'Блок живлення', label: cart_data.power.data.label, price: `${cart_data.power.price}₴`, href: cart_data.power.href, key: '6' });
    }
    if (cart_data?.hdd?.data) {
      data.push({ type: 'Жорсткий диск', label: cart_data.hdd.data.label, price: `${cart_data.hdd.price}₴`, href: cart_data.hdd.href, key: '7' });
    }
    if (cart_data?.ssd1?.data) {
      data.push({ type: 'SSD диск', label: cart_data.ssd1.data.label, price: `${cart_data.ssd1.price}₴`, href: cart_data.ssd1.href, key: '8' });
    }
    if (cart_data?.ssd2?.data) {
      data.push({ type: 'SSD диск 2', label: cart_data.ssd2.data.label, price: `${cart_data.ssd2.price}₴`, href: cart_data.ssd2.href, key: '9' });
    }
    if (cart_data?.case?.data) {
      data.push({ type: 'Корпус', label: cart_data.case.data.label, price: `${cart_data.case.price}₴`, href: cart_data.case.href, key: '10' });
    }
    setTableData(data);
    setTotalCost(data.reduce((acc, obj) => acc + parseInt(obj.price.replace('₴', '')), 0));
  }, []);

  const onRowClick = (data) => {
    window.open(data.href, '_blank', 'noreferrer');
  };

  const onSubmit = (formdata) => {
    const data = {
      processor: cartData.processor.href,
      cooler: cartData.cooler.href,
      motherboard: cartData.motherboard.href,
      ram: cartData.ram.href,
      power: cartData.power.href,
      pcCase: cartData.case.href,
      videocard: cartData?.videocard?.href || 'Без відеокарти',
      hdd: cartData?.hdd?.href || 'Без жорсткого диску',
      ssd: cartData?.ssd1?.href || 'Без SSD диску',
      ssd2: cartData?.ssd2?.href || 'Без SSD диску',
      email: formdata.email,
    };

    emailjs.send('service_fzcpkxc', 'template_rpio8xq', data, 'user_xF9B2bI6TfnIIj3DRC5xs')
      .then(() => {
        if (emailSave) {
          localStorage.setItem('email', JSON.stringify(formdata.email));
          localStorage.setItem('emailSave', JSON.stringify(emailSave));
        }
        else {
          localStorage.setItem('email', JSON.stringify(''));
          localStorage.setItem('emailSave', JSON.stringify(false));
        };
        notify();
        setModalIsOpen(false);
      }, (error) => {
         alert(error.text);
      });
  };

  const notify = () => toast.custom(
    <CustomToast
      title="Ваша збірка успішно надіслана на пошту"
      id="successCart"
      theme='success'
    />, { id: 'successCart' }
  );

  const openModal = () => {
    clearErrors(); 
    setModalIsOpen(true); 
    setValue('email', JSON.parse(localStorage.getItem('email')) || '');
  };

  const clearCart = () => {
    localStorage.setItem('cartData', JSON.stringify(''))
  }

  return (
    <div className="menuPageContainer">
      <div className="titleArea">
        <div className="mainTitle">Корзина</div>
        <div className="subTitle">Натисніть на елемент таблиці для того щоб перейти на сторінку покупки товару</div>
      </div>
      {cartData && tableData.length > 0 ? 
        <div>
          <CustomTable columns={columns} data={tableData} onClick={onRowClick}/>
          <LineDivider/>
          {windowWidth > 735 ? 
            <div style={{ marginBottom: '-77px', width: '240px' }}>
              <CustomButton onClick={() => clearCart()} title='Очистити корзину'/> 
            </div>
            : <></>
          }
          <div className='cartPrice'>
            <div className='cartText'>
              Загальна вартість: <span style={{ color: '#f84147' }}>{totalCost}₴</span>
            </div>
          </div>
          <div className='cartButton'>
            <CustomButton onClick={() => openModal()} title='Надіслати збірку на пошту'/>
            {windowWidth <= 735 ? 
              <div>
                <LineDivider style={{ marginTop: '20px', marginBottom: '20px' }} />
                <CustomButton onClick={() => clearCart()} title='Очистити корзину'/>
              </div> : <></>
            }
          </div>
        </div> 
        :
        <div className="cartEmpty">Наразі корзина порожня</div>
      }
      <CustomModal 
        modalIsOpen={modalIsOpen} 
        setIsOpen={setModalIsOpen} 
        height='230px'
        children={
          <form onSubmit={handleSubmit(onSubmit)}>
            <InputWithIcon
              icon={EmailIcon}
              placeholder={'Введіть вашу пошту'}
              label='email'
              register={register}
              isFormInput={true}
              inputStyle={{ height: '48px' }}
              error={errors.email}
              pattern={{
                required: "Це поле обов'язковє",
                pattern: {
                    // eslint-disable-next-line
                    value: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
                    message: "Пошта введенна невірно"
                }
              }}
            />
            <Switch 
              isOn={emailSave} 
              handleToggle={setEmailSave} 
              title='Зберегти пошту'
              id={'emailSave'}
            />
            <br></br>
            <CustomButton 
              onClick={handleSubmit(onSubmit)} 
              style={!isValid ? {opacity: '0.5'} : {}} 
              title='Надіслати'
            />
          </form>
        } 
      />
      <Toaster/>
    </div>
  )
}

export default CartPage;