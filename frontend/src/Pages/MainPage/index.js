import React, { useState, useEffect} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import CustomMultiSelect from '../../Components/CustomMultiSelect';
import RadioInput from '../../Components/RadioInput';
import { mainActions } from '../../Globals/MainPage/actions';
import { 
  processors, 
  coolers,
  motherboards,
  rams,
  videocards,
  ssds,
  hdds,
  powers,
  cases,
  maxVideocard1660,
  maxVideocard3050,
  maxVideocard3060ti,
  maxVideocard3070ti,
  maxVideocard4070,
  maxVideocard4070ti,
  maxVideocard4080,
  maxVideocard4090,
  maxVideocard1660_All,
  maxVideocard3050_All,
  maxVideocard3060ti_All,
  maxVideocard3070ti_All,
  maxVideocard4070_All,
  maxVideocard4070ti_All,
  maxVideocard4080_All,
  maxVideocard4090_All
} from './options';
import './styles.scss';
import CustomButton from '../../Components/CustomButton';
import LineDivider from '../../Components/LineDivider';
import CustomToast from '../../Components/CustomToast';
import toast, { Toaster } from 'react-hot-toast';
import Switch from '../../Components/Switch';

const MainPage = () => {
    const dispatch = useDispatch();
    const processorData = useSelector((state) => state.main.processorData);
    const processorsData = useSelector((state) => state.main.processors);
    const coolerData = useSelector((state) => state.main.coolerData);
    const coolersData = useSelector((state) => state.main.coolers);
    const motherboardData = useSelector((state) => state.main.motherboardData);
    const motherboardsData = useSelector((state) => state.main.motherboards);
    const ramData = useSelector((state) => state.main.ramData);
    const ramsData = useSelector((state) => state.main.rams);
    const powerData = useSelector((state) => state.main.powerData);
    const powersData = useSelector((state) => state.main.powers);
    const videocardData = useSelector((state) => state.main.videocardData);
    const videocardsData = useSelector((state) => state.main.videocards);
    const ssd1Data = useSelector((state) => state.main.ssd1Data);
    const ssd2Data = useSelector((state) => state.main.ssd2Data);
    const hddData = useSelector((state) => state.main.hddData);
    const HDDsData = useSelector((state) => state.main.HDDs);
    const SSDsData = useSelector((state) => state.main.SSDs);
    const SSD2sData = useSelector((state) => state.main.SSD2s);
    const caseData = useSelector((state) => state.main.caseData);
    const casesData = useSelector((state) => state.main.cases);
    const [processor, selectedProcessor] = useState();
    const [processorValue, setProcessorValue] = useState();
    const [cooler, selectedCooler] = useState();
    const [coolerValue, setCoolerValue] = useState();
    const [motherboard, selectedMotherboard] = useState();
    const [motherboardValue, setMotherboardValue] = useState();
    const [ram, selectedRam] = useState();
    const [ramValue, setRamValue] = useState();
    const [power, selectedPower] = useState();
    const [powerValue, setPowerValue] = useState();
    const [videocard, selectedVideocard] = useState();
    const [videocardValue, setVideocardValue] = useState();
    const [hdd, selectedHdd] = useState();
    const [hddValue, setHddValue] = useState();
    const [ssd, selectedSsd] = useState();
    const [ssdValue, setSsdValue] = useState();
    const [ssd2, selectedSsd2] = useState();
    const [ssd2Value, setSsd2Value] = useState();
    const [pcCase, selectedCase] = useState();
    const [pcCaseValue, setCaseValue] = useState();
    const [processorDescriptionLength, setProcessorDescriptionLength] = useState(600);
    const [coolerDescriptionLength, setCoolerDescriptionLength] = useState(400);
    const [motherboardDescriptionLength, setMotherboardDescriptionLength] = useState(600);
    const [ramDescriptionLength, setRamDescriptionLength] = useState(400);
    const [videocardDescriptionLength, setVideocardDescriptionLength] = useState(600);
    const [powerDescriptionLength, setPowerDescriptionLength] = useState(400);
    const [hddDescriptionLength, setHddDescriptionLength] = useState(400);
    const [ssd1DescriptionLength, setSsd1DescriptionLength] = useState(400);
    const [ssd2DescriptionLength, setSsd2DescriptionLength] = useState(400);
    const [caseDescriptionLength, setCaseDescriptionLength] = useState(400);
    const [processorsForVideocard, setProcessorsForVideocard] = useState([]);
    const [totalPower, setTotalPower] = useState();
    const [onlyBestVideocards, setOnlyBestVideocards] = useState(false)

    useEffect(() => {
      const ramPower = 10;
      const hddPower = hddValue?.length > 0 && hdd ? 15 : 0;
      const ssd1Power = ssdValue?.length > 0 && ssd ? ssd1Data?.data?.isM2 ? 10 : 15 : 0;
      const ssd2Power = ssd2Value?.length > 0 && ssd2 ? ssd2Data?.data?.isM2 ? 10 : 15 : 0;
      const coolerPower = pcCaseValue?.length > 0 && pcCase ? caseData?.data?.coolers * 5 : 0;
      const motherboardPower = motherboardValue?.length > 0 && motherboard && processorData?.data?.label?.slice(0, 3) === 'AMD' ? 50 : 0;
      const processorCoolerPower = coolerValue?.length > 0 && cooler ? coolerData?.data?.value === 'water_cooler' ? 25 : 5 : 0;
      const processorPower = processorValue?.length > 0 && processor ? processorData?.data?.power + processorCoolerPower : 0;
      const videocardPower = videocardValue?.length > 0 && videocard  ? videocardData?.data?.power : 0;
      const total_power = ramPower + hddPower + ssd1Power + ssd2Power + coolerPower + motherboardPower + processorPower + videocardPower;

      setTotalPower(total_power);

      if (powerData?.data?.value < total_power * 1.2) {
        selectedPower();
      };
    }, [
          processorData, 
          videocardData, 
          coolerData, 
          caseData,
          ssd1Data,
          ssd2Data, 
          hdd,
          ssd,
          ssd2,
          pcCase,
          motherboard,
          cooler,
          processor,
          videocard,
          powerData,
          pcCaseValue,
          hddValue,
          ssdValue,
          ssd2Value,
          videocardValue,
          processorValue,
          motherboardValue,
          coolerValue
      ]
    );

    const setProcessor = (data) => {
      setProcessorValue(data);
      setProcessorDescriptionLength(600);
      selectedProcessor();

      if (ramValue?.length > 0 && ramValue[0].value === '8' 
        && ['i5', 'i7', 'i9', 'ryzen5', 'ryzen7', 'ryzen9'].includes(data[0].value)) {
        selectedRam();
      };

      const maxVideocard_Best = 
        maxVideocard1660.videocards.includes(videocardData?.data?.value) ? maxVideocard1660 :
        maxVideocard3050.videocards.includes(videocardData?.data?.value) ? maxVideocard3050 :
        maxVideocard3060ti.videocards.includes(videocardData?.data?.value) ? maxVideocard3060ti :
        maxVideocard3070ti.videocards.includes(videocardData?.data?.value) ? maxVideocard3070ti :
        maxVideocard4070.videocards.includes(videocardData?.data?.value) ? maxVideocard4070 :
        maxVideocard4070ti.videocards.includes(videocardData?.data?.value) ? maxVideocard4070ti :
        maxVideocard4080.videocards.includes(videocardData?.data?.value) ? maxVideocard4080 :
        maxVideocard4090.videocards.includes(videocardData?.data?.value) ? maxVideocard4090 : '';

      const maxVideocard_All = 
        maxVideocard1660_All.videocards.includes(videocardData?.data?.value) ? maxVideocard1660_All :
        maxVideocard3050_All.videocards.includes(videocardData?.data?.value) ? maxVideocard3050_All :
        maxVideocard3060ti_All.videocards.includes(videocardData?.data?.value) ? maxVideocard3060ti_All :
        maxVideocard3070ti_All.videocards.includes(videocardData?.data?.value) ? maxVideocard3070ti_All :
        maxVideocard4070_All.videocards.includes(videocardData?.data?.value) ? maxVideocard4070_All :
        maxVideocard4070ti_All.videocards.includes(videocardData?.data?.value) ? maxVideocard4070ti_All :
        maxVideocard4080_All.videocards.includes(videocardData?.data?.value) ? maxVideocard4080_All :
        maxVideocard4090_All.videocards.includes(videocardData?.data?.value) ? maxVideocard4090_All : '';

      const maxVideocard = onlyBestVideocards ? maxVideocard_Best : maxVideocard_All;
      setProcessorsForVideocard( maxVideocard?.processors || []);

      dispatch(mainActions.getProcessors({value: data[0].value, processorsLabels: maxVideocard?.processors || ''}));
    };

    const handleChangeProcessor = (data) => {
      selectedProcessor(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'processor'}));
      setProcessorDescriptionLength(600);

      if (coolerValue?.length > 0) {
        dispatch(mainActions.getCoolers({
          value: coolerValue[0].value, 
          socket: data.socket,
          tdp: (data.power * 0.9).toFixed(0) || ''
        }));
      };

      if (!coolerData?.data?.sockets.includes(data.socket)) {
        selectedCooler();
      };

      const intelValues = ['i3', 'i5', 'i7', 'i9'];
      let chipsetNumber = 3;
      let chipsets = '';

      if (intelValues.includes(data.value)) {
        chipsetNumber = 
          data.label === 'Intel Core i3 8350K 4000 МГц' ? 3 : 
          data.label.split('-')[1].slice(0,2) - 6
        chipsets = 
          data.value === 'i3' ? [`H${chipsetNumber}10`, `H${chipsetNumber}70`, `B${chipsetNumber}60`, `Z${chipsetNumber}90`] :
          data.value === 'i5' ? [`H${chipsetNumber}70`, `B${chipsetNumber}60`, `Z${chipsetNumber}90`] :
          data.value === 'i7' ? [`B${chipsetNumber}60`, `Z${chipsetNumber}90`] :
          data.value === 'i9' ? [`Z${chipsetNumber}90`] : ''
      }
      else if (data.socket === 'AM4'){
        chipsets = 
          data.label === 'AMD Ryzen 3 3200G 3600 МГц' || data.label === 'AMD Ryzen 3 1200AF 3200 МГц' ? [`X470`, `B450`, `A320`] :
          data.value === 'ryzen3' ||  data.label === 'AMD Ryzen 5 3600 3600 МГц' ? [`X470`, 'X570', `B450`, `B550`, 'A520'] :
          data.value === 'ryzen5' || data.value === 'ryzen7' ? [`X470`, 'X570', `B450`, `B550`] :
          data.value === 'ryzen9' ? ['X570', `B550`]  : ''
      };

      if (motherboardValue?.length > 0) {
        dispatch(mainActions.getMotherboards({value: motherboardValue[0].value, socket: data.socket, chipsets: chipsets}));
      };

      if (motherboardData?.data?.socket !== data.socket || !chipsets.includes((motherboardData?.data?.chipset))) {
        selectedMotherboard();
      };

      const memoryType = 
        data.socket === 'AM5' || 
        data.label.split('-')[1].slice(0,2) === '13' ? 'DDR5' : 'DDR4' 

      if (ramValue?.length > 0) {
        dispatch(mainActions.getRams({value: ramValue[0].value, memoryType: memoryType}));
      };

      if (memoryType !== ramData?.data?.memoryType) {
        selectedRam();
      };
    };

    const setCooler = (data) => {
      setCoolerValue(data);
      dispatch(mainActions.getCoolers({
        value: data[0].value, 
        socket: processorData?.data?.socket || '', 
        tdp: (processorData?.data?.power * 0.9).toFixed(0) || ''
      }));
      selectedCooler();
      setCoolerDescriptionLength(400);
    };

    const handleChangeCooler = (data) => {
      selectedCooler(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'cooler'}));
      setCoolerDescriptionLength(400);

      if (caseData?.data?.coolerMaxWidth <= data.width) {
        selectedCase();
      };

      if (pcCaseValue?.length > 0) {
        dispatch(mainActions.getCases({value: pcCaseValue[0].value, videocardWidth: videocardData?.data?.width || '', coolerWidth: data.width }));
      };
    };

    const setMotherboard = (data) => {
      setMotherboardValue(data);
      setMotherboardDescriptionLength(600);
      selectedMotherboard();

      let chipsets = '';
      if (processorData?.data) {
        const intelValues = ['i3', 'i5', 'i7', 'i9'];
        let chipsetNumber = 3;

        if (intelValues.includes(processorData?.data?.value)) {
          chipsetNumber = 
            processorData?.data?.label === 'Intel Core i3 8350K 4000 МГц' ? 3 : 
            processorData?.data?.label.split('-')[1].slice(0,2) - 6
            chipsets = 
              processorData?.data?.value === 'i3' ? [`H${chipsetNumber}10`, `H${chipsetNumber}70`, `B${chipsetNumber}60`, `Z${chipsetNumber}90`] :
              processorData?.data?.value === 'i5' ? [`H${chipsetNumber}70`, `B${chipsetNumber}60`, `Z${chipsetNumber}90`] :
              processorData?.data?.value === 'i7' ? [`B${chipsetNumber}60`, `Z${chipsetNumber}90`] :
              processorData?.data?.value === 'i9' ? [`Z${chipsetNumber}90`] : ''
        }
        else if (data.socket === 'AM4') {
          chipsets = 
            processorData?.data?.label === 'AMD Ryzen 3 3200G 3600 МГц' || processorData?.data?.label === 'AMD Ryzen 3 1200AF 3200 МГц' ? [`X470`, `B450`, `A320`] :
            processorData?.data?.value === 'ryzen3' ||  processorData?.data?.label === 'AMD Ryzen 5 3600 3600 МГц' ? [`X470`, 'X570', `B450`, `B550`, 'A520'] :
            processorData?.data?.value === 'ryzen5' || processorData?.data?.value === 'ryzen7' ? [`X470`, 'X570', `B450`, `B550`] :
            processorData?.data?.value === 'ryzen9' ? ['X570', `B550`]  : ''
        };
      };

      dispatch(mainActions.getMotherboards({value: data[0].value, socket: processorData?.data?.socket || '', chipsets: chipsets}));
    };

    const handleChangeMotherboard = (data) => {
      selectedMotherboard(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'motherboard'}));
      setMotherboardDescriptionLength(600);
    };
    
    const setVideocard = (data) => {
      setVideocardValue(data);
      dispatch(mainActions.getVideocards({value: data[0].value}));
      selectedVideocard();
      setVideocardDescriptionLength(600);
    };

    const handleChangeVideocard = (data) => {
      selectedVideocard(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'videocard'}));
      setVideocardDescriptionLength(600);

      if (caseData?.data?.videocardMaxWidth <= data.width) {
        selectedCase();
      };

      if (pcCaseValue?.length > 0) {
        dispatch(mainActions.getCases({value: pcCaseValue[0].value, videocardWidth: data.width, coolerWidth: coolerData?.data?.width || '' }));
      };

      const maxVideocard_Best = 
        maxVideocard1660.videocards.includes(data.value) ? maxVideocard1660 :
        maxVideocard3050.videocards.includes(data.value) ? maxVideocard3050 :
        maxVideocard3060ti.videocards.includes(data.value) ? maxVideocard3060ti :
        maxVideocard3070ti.videocards.includes(data.value) ? maxVideocard3070ti :
        maxVideocard4070.videocards.includes(data.value) ? maxVideocard4070 :
        maxVideocard4070ti.videocards.includes(data.value) ? maxVideocard4070ti :
        maxVideocard4080.videocards.includes(data.value) ? maxVideocard4080 :
        maxVideocard4090;

      const maxVideocard_All = 
        maxVideocard1660_All.videocards.includes(data.value) ? maxVideocard1660_All :
        maxVideocard3050_All.videocards.includes(data.value) ? maxVideocard3050_All :
        maxVideocard3060ti_All.videocards.includes(data.value) ? maxVideocard3060ti_All :
        maxVideocard3070ti_All.videocards.includes(data.value) ? maxVideocard3070ti_All :
        maxVideocard4070_All.videocards.includes(data.value) ? maxVideocard4070_All :
        maxVideocard4070ti_All.videocards.includes(data.value) ? maxVideocard4070ti_All :
        maxVideocard4080_All.videocards.includes(data.value) ? maxVideocard4080_All :
        maxVideocard4090_All;

      const maxVideocard = onlyBestVideocards ? maxVideocard_Best : maxVideocard_All;        
      setProcessorsForVideocard(maxVideocard.processors);

      if (processorValue?.length > 0) {
        dispatch(mainActions.getProcessors({value: processorValue[0].value, processorsLabels: maxVideocard.processors}));
      };

      if (!maxVideocard.processors.includes(processorData.data.label)) {
        selectedProcessor();
      };
    };

    const setRam = (data) => {
      setRamValue(data);
      selectedRam();
      setRamDescriptionLength(400);

      let memoryType = '';
      if (processorData?.data) {
        memoryType = 
          processorData?.data?.socket === 'AM5' || 
          processorData?.data?.label.split('-')[1].slice(0,2) === '13' ? 'DDR5' : 'DDR4'  
      };

      dispatch(mainActions.getRams({value: data[0].value, memoryType: memoryType}));
    };

    const handleChangeRam = (data) => {
      selectedRam(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'ram'}));
      setRamDescriptionLength(400);
    };
    
    const setPower = (data) => {
      setPowerValue(data);
      dispatch(mainActions.getPowers({value: data[0].value}));
      selectedPower();
      setPowerDescriptionLength(400);
    };

    const handleChangePower = (data) => {
      selectedPower(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'power'}));
      setPowerDescriptionLength(400);
    };

    const setHDD = (data) => {
      setHddValue(data);
      dispatch(mainActions.getHDDs({value: data[0].value, isSSD: false}));
      selectedHdd();
      setHddDescriptionLength(400);
    };

    const handleChangeHDD = (data) => {
      selectedHdd(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'hdd'}));
      setHddDescriptionLength(400);
    };

    const setSSD = (data) => {
      setSsdValue(data);
      dispatch(mainActions.getSSDs({value: data[0].value, isSSD: true}));
      selectedSsd();
      setSsd1DescriptionLength(400);
    };

    const handleChangeSSD = (data) => {
      selectedSsd(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'ssd1'}));
      setSsd1DescriptionLength(400);
    };

    const setSSD2 = (data) => {
      setSsd2Value(data);
      dispatch(mainActions.getSSD2s({value: data[0].value, isSSD: true}));
      selectedSsd2();
      setSsd2DescriptionLength(400);
    };

    const handleChangeSSD2 = (data) => {
      selectedSsd2(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId, data: data, pageType: 'ssd2'}));
      setSsd2DescriptionLength(400);
    };

    const setCase = (data) => {
      setCaseValue(data);
      selectedCase();
      setCaseDescriptionLength(400);
      dispatch(mainActions.getCases({
        value: data[0].value,           
        coolerWidth: coolerData?.data?.width || '', 
        videocardWidth: videocardData?.data?.width || '',
      }));
    };

    const handleChangeCase = (data) => {
      selectedCase(data.rozetkaId);
      dispatch(mainActions.getPage({id: data.rozetkaId,  data: data, pageType: 'case'}));
      setCaseDescriptionLength(400);
    };

    const checkFormValidation = () => {
      const rom = hdd || ssd || ssd2
      return processor && cooler && motherboard && pcCase && power && ram && rom
    };

    const notify = () => toast.custom(
      <CustomToast
        title="Ваша збірка успішно додана до корзини"
        id="successCart"
        cart={true}
        theme='success'
      />, { id: 'successCart' }
    );

    const notifyError = () => toast.custom(
      <CustomToast
        title="Для того щоб додати збірку до корзини, будь ласка, заповніть усі поля або оберіть опцію (без), де це можливо"
        id="errorCart"
      />, { id: 'errorCart' }
    );

    const addToCart = () => {
      localStorage.setItem('cartData', JSON.stringify(
        {
          processor: processorData,
          cooler: coolerData,
          motherboard: motherboardData,
          ram: ramData,
          power: powerData,
          pcCase: caseData,
          videocard: videocardData,
          hdd: hddData,
          ssd1: ssd1Data,
          ssd2: ssd2Data,
        }
      ));
      notify();
    };

    const handletoggleOnlyBestVideocards = (data) => {
      setOnlyBestVideocards(data)
      const maxVideocard_Best = 
        maxVideocard1660.videocards?.includes(videocardData?.data?.value) ? maxVideocard1660 :
        maxVideocard3050.videocards?.includes(videocardData?.data?.value) ? maxVideocard3050 :
        maxVideocard3060ti.videocards?.includes(videocardData?.data?.value) ? maxVideocard3060ti :
        maxVideocard3070ti.videocards?.includes(videocardData?.data?.value) ? maxVideocard3070ti :
        maxVideocard4070.videocards?.includes(videocardData?.data?.value) ? maxVideocard4070 :
        maxVideocard4070ti.videocards?.includes(videocardData?.data?.value) ? maxVideocard4070ti :
        maxVideocard4080.videocards?.includes(videocardData?.data?.value) ? maxVideocard4080 :
        maxVideocard4090.videocards?.includes(videocardData?.data?.value) ? maxVideocard4090 : '';

      const maxVideocard_All = 
        maxVideocard1660_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard1660_All :
        maxVideocard3050_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard3050_All :
        maxVideocard3060ti_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard3060ti_All :
        maxVideocard3070ti_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard3070ti_All :
        maxVideocard4070_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard4070_All :
        maxVideocard4070ti_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard4070ti_All :
        maxVideocard4080_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard4080_All :
        maxVideocard4090_All.videocards?.includes(videocardData?.data?.value) ? maxVideocard4090_All : '';

      const maxVideocard = data ? maxVideocard_Best : maxVideocard_All;      
      setProcessorsForVideocard(maxVideocard.processors);

      if (processorValue?.length > 0) {
        dispatch(mainActions.getProcessors({value: processorValue[0]?.value, processorsLabels: maxVideocard?.processors}));
      };

      if (!maxVideocard.processors?.includes(processorData?.data?.label)) {
        selectedProcessor();
      };
    }
    
    return (
        <div className="menuPageContainer">
          <div className="titleArea">
            <div className="mainTitle">Конфігуратор ПК</div>
            <div className="subTitle">Помічник з підбору комплектуючих</div>
          </div>
          <div className='mainPageFlex'>
            <div className='table'>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Процесор</div>
                  {processorValue?.length > 0 && videocardData?.data ?
                  <div className='processorSwitch'>
                    <p className="subTitle">
                      <span style={{color: 'black', fontSize: '15px'}}>Ця опція фільтрує процесори:</span>
                      <br></br><span style={{color: '#227093'}}>Якщо не вибрано:</span> всі процесори які можна використовувати з відеокартою
                      <br></br><span style={{color: '#227093'}}>Якщо вибрано:</span> тільки процесори які максимально сумісні з відеокартою
                    </p>
                    <br></br>
                    <Switch 
                      title='Тільки сумісні с відеокартою' 
                      isOn={onlyBestVideocards}
                      handleToggle={(data) => handletoggleOnlyBestVideocards(data)}
                      id={'onlyBestVideocards'}
                    />
                  </div> : <></>
                  }
                  <CustomMultiSelect 
                    className='select'
                    options={processors}
                    selected={processorValue}
                    setSelected={(value) => setProcessor(value)}
                  />
                </div>
                {processorValue?.length > 0 ? processorsData.length > 0 ?
                  <div className='tableRadio'>
                    {processorsData.map(processorData => (
                      <RadioInput
                        label={processorData.label}
                        value={processorData.rozetkaId}
                        id={processorData._id}
                        handleChange={() => handleChangeProcessor(processorData)}
                        isChecked={processor}
                      />
                    ))}
                  </div> : 
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Немає підходящих процесорів під вибрану відеокарту, 
                    для цієї відеокарти потрібен один з цих процесорів ({processorsForVideocard?.join(', ')})
                  </div> : <></>  
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Охолодження</div>
                  <CustomMultiSelect 
                    className='select'
                    options={coolers}
                    selected={coolerValue}
                    setSelected={(value) => setCooler(value)}
                  />
                </div>
                {coolerValue?.length > 0 ? coolersData.length > 0 ?
                  <div className='tableRadio'>
                    {coolersData.map(coolerData => (
                      <RadioInput
                        label={coolerData.label}
                        value={coolerData.rozetkaId}
                        id={coolerData._id}
                        handleChange={() => handleChangeCooler(coolerData)}
                        isChecked={cooler}
                      />
                    ))}
                  </div> : 
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Немає підходящих систем повітряного охолодження під вибраний процесор (TDP процесора: {processorData.data.power}W),
                    виберіть водяне охолодження
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Материнська плата</div>
                  <CustomMultiSelect 
                    className='select'
                    options={motherboards}
                    selected={motherboardValue}
                    setSelected={(value) => setMotherboard(value)}
                  />
                </div>
                {motherboardValue?.length > 0 ? motherboardsData.length > 0 ? 
                  <div className='tableRadio'>
                    {motherboardsData.map(motherboardData => (
                      <RadioInput
                        label={motherboardData.label}
                        value={motherboardData.rozetkaId}
                        id={motherboardData._id}
                        handleChange={() => handleChangeMotherboard(motherboardData)}
                        isChecked={motherboard}
                      />
                    ))}
                  </div> :   
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Немає підходящих материнських плат цієї фірми під вибраний процесор (чипсет),
                    виберіть іншу фірму
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Оперативна пам'ять</div>
                  <CustomMultiSelect 
                    className='select'
                    options={rams}
                    selected={ramValue}
                    setSelected={(value) => setRam(value)}
                  />
                </div>
                {ramValue?.length > 0 ? ramsData.length > 0 ? 
                  processorValue?.length > 0 && ramValue[0].value === '8' 
                  && ['i5', 'i7', 'i9', 'ryzen5', 'ryzen7', 'ryzen9'].includes(processorValue[0].value) ?
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Для вашої системи мінімальна кількість оперативної пам'яті дорівнює 16 ГБ
                  </div> :
                  <div className='tableRadio'>
                    {ramsData.map(ramData => (
                      <RadioInput
                        label={ramData.label}
                        value={ramData.rozetkaId}
                        id={ramData._id}
                        handleChange={() => handleChangeRam(ramData)}
                        isChecked={ram}
                      />
                    ))}
                  </div> : 
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Немає оперативної пам'яті цієї ємності яка відповідає типу DDR5,
                    мінімальна кількість оперативної пам'яті з DDR5 дорівнює 32 ГБ
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Відеокарта</div>
                  <CustomMultiSelect 
                    className='select'
                    options={videocards}
                    selected={videocardValue}
                    setSelected={(value) => setVideocard(value)}
                  />
                </div>
                {videocardValue?.length > 0 ? 
                  <div className='tableRadio'>
                    {videocardsData.map(videocardData => (
                      <RadioInput
                        label={videocardData.label}
                        value={videocardData.rozetkaId}
                        id={videocardData._id}
                        handleChange={() => handleChangeVideocard(videocardData)}
                        isChecked={videocard}
                      />
                    ))}
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Блок живлення</div>
                  <CustomMultiSelect 
                    className='select'
                    options={powers}
                    selected={powerValue}
                    setSelected={(value) => setPower(value)}
                  />
                </div>
                {powerValue?.length > 0 ? 
                  powerValue[0].value < totalPower * 1.2 - 10 ? 
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Виберіть інше значення потужності блоку живлення, що відповідає споживанню вашої системи {totalPower}W + 20%,
                    тобто мінмальна потужність блока живлення повинна бути {Math.round(totalPower * 1.2 > 1050 ? 1200 / 100 : totalPower * 1.2 / 100) * 100}
                  </div>
                  :
                  <div className='tableRadio'>
                    {powersData.map(powerData => (
                      <RadioInput
                        label={powerData.label}
                        value={powerData.rozetkaId}
                        id={powerData._id}
                        handleChange={() => handleChangePower(powerData)}
                        isChecked={power}
                      />
                    ))}
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">Жорсткий диск</div>
                  <CustomMultiSelect 
                    className='select'
                    options={hdds}
                    selected={hddValue}
                    setSelected={(value) => setHDD(value)}
                  />
                </div>
                {hddValue?.length > 0 ? 
                  <div className='tableRadio'>
                    {HDDsData.map(hddData => (
                      <RadioInput
                        label={hddData.label}
                        value={hddData.rozetkaId}
                        id={hddData._id}
                        handleChange={() => handleChangeHDD(hddData)}
                        isChecked={hdd}
                      />
                    ))}
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">SSD диск</div>
                  <CustomMultiSelect 
                    className='select'
                    options={ssds}
                    selected={ssdValue}
                    setSelected={(value) => setSSD(value)}
                  />
                </div>
                {ssdValue?.length > 0 ? 
                  <div className='tableRadio'>
                    {SSDsData.map(ssdData1 => (
                      <RadioInput
                        label={ssdData1.label}
                        value={ssdData1.rozetkaId}
                        id={ssdData1._id}
                        handleChange={() => handleChangeSSD(ssdData1)}
                        isChecked={ssd}
                      />
                    ))}
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem'>
                  <div className="secondTitle">SSD диск 2</div>
                  <CustomMultiSelect 
                    className='select'
                    options={ssds}
                    selected={ssd2Value}
                    setSelected={(value) => setSSD2(value)}
                  />
                </div>
                {ssd2Value?.length > 0 ? 
                  <div className='tableRadio'>
                    {SSD2sData.map(ssd2Data => (
                      <RadioInput
                        label={ssd2Data.label}
                        value={ssd2Data.rozetkaId}
                        id={ssd2Data._id + '1'}
                        handleChange={() => handleChangeSSD2(ssd2Data)}
                        isChecked={ssd2}
                      />
                    ))}
                  </div> : <></>
                }
              </div>
              <div className='tableCollumn'>
                <div className='tableItem' style={{ marginBottom: '20px' }}>
                  <div className="secondTitle">Корпус</div>
                  <CustomMultiSelect 
                    className='select'
                    options={cases}
                    selected={pcCaseValue}
                    setSelected={(value) => setCase(value)}
                  />
                </div>
                {pcCaseValue?.length > 0 ? casesData.length > 0 ?
                  <div className='tableRadio'>
                    {casesData.map(caseData => (
                      <RadioInput
                        label={caseData.label}
                        value={caseData.rozetkaId}
                        id={caseData._id}
                        handleChange={() => handleChangeCase(caseData)}
                        isChecked={pcCase}
                      />
                    ))}
                  </div> : 
                  <div className='subTitle' style={{ maxWidth: '440px', marginTop: '20px' }}>
                    Немає підходящих корпусів цієї фірми які відповідають вибраним комплектуючим
                  </div> : <></>
                }
              </div>
            </div>
            <div className='buildContainer'>
              <div className="secondTitle" style={{ fontSize: 22 }}>Ваша збірка</div>
              <div style={{ marginTop: '20px' }}>
                {processorValue?.length > 0 && processor ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='processor' style={{ maxHeight: '150px' }} src={processorData.images ? processorData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{processorData?.data?.label}</div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Тип роз'єму: </p>
                        <p className="CharSubTitle">{processorData?.data?.socket}</p>
                      </div>
                      {processorData?.data?.coreArchitecture ? <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Архітектура процесора: </p>
                        <p className="CharSubTitle">{processorData?.data?.coreArchitecture}</p>
                      </div> : <></>}
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Кількість ядер: </p>
                        <p className="CharSubTitle">{processorData?.data?.cores}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Кількість потоків: </p>
                        <p className="CharSubTitle">{processorData?.data?.threads}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Інтегрована графіка: </p>
                        <p className="CharSubTitle">
                          {processorData?.data?.integratedGraphics ? processorData?.data.integratedGraphics : 'Немає'}
                        </p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Максимальна частота процесора: </p>
                        <p className="CharSubTitle">{processorData?.data?.maxFrequency} МГц</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Обсяг кеш пам'яті 3 рівня: </p>
                        <p className="CharSubTitle">{processorData?.data?.L3cache}</p>
                      </div>
                      <br></br>
                      <div 
                        dangerouslySetInnerHTML={{ __html: `${processorData?.description?.text.slice(0, processorDescriptionLength)}
                        ${processorData?.description?.text.length >= 600 && processorDescriptionLength === 600 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {processorData?.description?.text.length >= 600 ?
                        <button className='accessoriesButton' onClick={() => setProcessorDescriptionLength(processorDescriptionLength === 600 ? 10000 : 600)}>
                          {processorDescriptionLength === 600 ? 'розгорнути' : 'згорнути' }
                        </button>
                        : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{processorData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {coolerValue?.length > 0 && cooler ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='cooler' style={{ maxHeight: '150px' }} src={coolerData.images ? coolerData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{coolerData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Максимальний TDP: </p>
                        <p className="CharSubTitle">{coolerData?.data?.TDP}</p>
                      </div>
                      <br></br>
                      <div 
                        dangerouslySetInnerHTML={{ __html: `${coolerData?.description?.text.slice(0, coolerDescriptionLength)} 
                        ${coolerData?.description?.text.length >= 600 && coolerDescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {coolerData?.description?.text.length >= 600 ?
                        <button className='accessoriesButton' onClick={() => setCoolerDescriptionLength(coolerDescriptionLength === 400 ? 10000 : 400)}>
                          {coolerDescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{processorData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {motherboardValue?.length > 0 && motherboard ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='motherboard' style={{ maxHeight: '150px' }} src={motherboardData.images ? motherboardData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{motherboardData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Сокет: </p>
                        <p className="CharSubTitle">{motherboardData?.data?.socket}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Чипсет: </p>
                        <p className="CharSubTitle">{motherboardData?.data?.chipset}</p>
                      </div>
                      <br></br>
                      <div 
                        dangerouslySetInnerHTML={{ __html: `${motherboardData?.description?.text.slice(0, motherboardDescriptionLength)} 
                        ${motherboardData?.description?.text.length >= 600 && motherboardDescriptionLength === 600 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {motherboardData?.description?.text.length >= 600 ?
                        <button className='accessoriesButton' onClick={() => setMotherboardDescriptionLength(motherboardDescriptionLength === 600 ? 10000 : 600)}>
                          {motherboardDescriptionLength === 600 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{motherboardData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {ramValue?.length > 0 && ram ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='ram' style={{ maxHeight: '150px' }} src={ramData.images ? ramData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{ramData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Обсяг пам'яті: </p>
                        <p className="CharSubTitle">{ramData?.data?.value} ГБ</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Тип пам'яті: </p>
                        <p className="CharSubTitle">{ramData?.data?.memoryType}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Частота пам'яті: </p>
                        <p className="CharSubTitle">{ramData?.data?.memoryFrequency}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Кількість планок: </p>
                        <p className="CharSubTitle">2</p>
                      </div>
                      <br></br>
                      <div 
                        dangerouslySetInnerHTML={{ __html: `${ramData?.description?.text.slice(0, ramDescriptionLength)} 
                        ${ramData?.description?.text.length >= 400 && ramDescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {ramData?.description?.text.length >= 400 ?
                        <button className='accessoriesButton' onClick={() => setRamDescriptionLength(ramDescriptionLength === 400 ? 10000 : 400)}>
                          {ramDescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{ramData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {videocardValue?.length > 0 && videocard ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='videocard' style={{ maxHeight: '150px' }} src={videocardData.images ? videocardData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{videocardData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Розрядність шини пам'яті: </p>
                        <p className="CharSubTitle">{videocardData?.data?.memoryBusWidth}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Частота пам'яті: </p>
                        <p className="CharSubTitle">{videocardData?.data?.memoryFrequency}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Частота ядра: </p>
                        <p className="CharSubTitle">{videocardData?.data?.coreFrequency}</p>
                      </div>
                      <br></br>
                      <div 
                        dangerouslySetInnerHTML={{ __html: `${videocardData?.description?.text.slice(0, videocardDescriptionLength)} 
                        ${videocardData?.description?.text.length >= 600 && videocardDescriptionLength === 600 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {videocardData?.description?.text.length >= 600 ?
                        <button className='accessoriesButton' onClick={() => setVideocardDescriptionLength(videocardDescriptionLength === 600 ? 10000 : 600)}>
                          {videocardDescriptionLength === 600 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{videocardData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {powerValue?.length > 0 && power ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='power' style={{ maxHeight: '150px' }} src={powerData.images ? powerData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{powerData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Потужність: </p>
                        <p className="CharSubTitle">{powerData?.data?.value} W</p>
                      </div>
                      <br></br>
                      <div 
                        dangerouslySetInnerHTML={{ __html: `${powerData?.description?.text.slice(0, powerDescriptionLength)} 
                        ${powerData?.description?.text.length >= 400 && powerDescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {powerData?.description?.text.length >= 400 ?
                        <button className='accessoriesButton' onClick={() => setPowerDescriptionLength(powerDescriptionLength === 400 ? 10000 : 400)}>
                          {powerDescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{powerData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {hddValue?.length > 0 && hdd ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='hdd' style={{ maxHeight: '150px' }} src={hddData.images ? hddData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{hddData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Обсяг пам'яті: </p>
                        <p className="CharSubTitle">{hddData?.data?.value} ГБ</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Швидкість передавання даних: </p>
                        <p className="CharSubTitle">{hddData?.data?.speed}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Швидкість обертання шпинделя: </p>
                        <p className="CharSubTitle">{hddData?.data?.spindleRotationSpeed}</p>
                      </div>
                      <br></br>
                      <div
                        dangerouslySetInnerHTML={{ __html: `${hddData?.description?.text.slice(0, hddDescriptionLength)} 
                        ${hddData?.description?.text.length >= 400 && hddDescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {hddData?.description?.text.length >= 400 ?
                        <button className='accessoriesButton' onClick={() => setHddDescriptionLength(hddDescriptionLength === 400 ? 10000 : 400)}>
                          {hddDescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{hddData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {ssdValue?.length > 0 && ssd ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='ssd1' style={{ maxHeight: '150px' }} src={ssd1Data.images ? ssd1Data?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{ssd1Data?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Форм-фактор: </p>
                        <p className="CharSubTitle">{ssd1Data?.data?.isM2 ? 'M.2' : '2.5"'}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Інтерфейс підключення: </p>
                        <p className="CharSubTitle">{ssd1Data?.data?.isM2 ? 'PCI Express 3.0 x4' : 'SATAIII'}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Обсяг пам'яті: </p>
                        <p className="CharSubTitle">{ssd1Data?.data?.value} ГБ</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Максимальна швидкість запису: </p>
                        <p className="CharSubTitle">{ssd1Data?.data?.writeSpeed}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Максимальна швидкість зчитування: </p>
                        <p className="CharSubTitle">{ssd1Data?.data?.readSpeed}</p>
                      </div>
                      <br></br>
                      <div
                        dangerouslySetInnerHTML={{ __html: `${ssd1Data?.description?.text.slice(0, ssd1DescriptionLength)} 
                        ${ssd1Data?.description?.text.length >= 400 && ssd1DescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {ssd1Data?.description?.text.length >= 400 ?
                        <button className='accessoriesButton' onClick={() => setSsd1DescriptionLength(ssd1DescriptionLength === 400 ? 10000 : 400)}>
                          {ssd1DescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{ssd1Data?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {ssd2Value?.length > 0 && ssd2 ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='ssd1' style={{ maxHeight: '150px' }} src={ssd2Data.images ? ssd2Data?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{ssd2Data?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Форм-фактор: </p>
                        <p className="CharSubTitle">{ssd2Data?.data?.isM2 ? 'M.2' : '2.5"'}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Інтерфейс підключення: </p>
                        <p className="CharSubTitle">{ssd2Data?.data?.isM2 ? 'PCI Express 3.0' : 'SATAIII'}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Обсяг пам'яті: </p>
                        <p className="CharSubTitle">{ssd2Data?.data?.value} ГБ</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Максимальна швидкість запису: </p>
                        <p className="CharSubTitle">{ssd2Data?.data?.writeSpeed}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Максимальна швидкість зчитування: </p>
                        <p className="CharSubTitle">{ssd2Data?.data?.readSpeed}</p>
                      </div>
                      <br></br>
                      <div
                        dangerouslySetInnerHTML={{ __html: `${ssd2Data?.description?.text.slice(0, ssd2DescriptionLength)} 
                        ${ssd2Data?.description?.text.length >= 400 && ssd2DescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {ssd2Data?.description?.text.length >= 400 ?
                        <button className='accessoriesButton' onClick={() => setSsd2DescriptionLength(ssd2DescriptionLength === 400 ? 10000 : 400)}>
                          {ssd2DescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{ssd2Data?.price}₴</p>
                      </div>
                    </div>
                  </div>
                </> : <></>
                }
                {pcCaseValue?.length > 0 && pcCase ?
                <>
                  <LineDivider/>
                  <div className='accessoriesContainer'>
                    <div className='accessoriesImage'>
                      <img alt='ssd1' style={{ maxHeight: '150px' }} src={caseData.images ? caseData?.images[0].base_action.url : ''}/>
                    </div>
                    <div style={{ marginLeft: '20px' }}>
                      <div className="accessoriesTitle">{caseData?.data?.label}</div>
                      <br></br>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Форм-фактор: </p>
                        <p className="CharSubTitle">{caseData?.data?.type}</p>
                      </div>
                      <div className="accessoriesCharacteristics">
                        <p className="CharTitle">Кількість вентиляторів: </p>
                        <p className="CharSubTitle">{caseData?.data?.coolers}</p>
                      </div>
                      <br></br>
                      <div
                        dangerouslySetInnerHTML={{ __html: `${caseData?.description?.text.slice(0, caseDescriptionLength)} 
                        ${caseData?.description?.text.length >= 400 && caseDescriptionLength === 400 ? '...' : '' }` }} 
                        style={{fontFamily: 'RobotoRegular', color: '#9aa1b8', maxWidth: '800px' }}
                      />
                      {caseData?.description?.text.length >= 400 ?
                        <button className='accessoriesButton' onClick={() => setCaseDescriptionLength(caseDescriptionLength === 400 ? 10000 : 400)}>
                          {caseDescriptionLength === 400 ? 'розгорнути' : 'згорнути' }
                        </button> : <></>
                      }
                      <br></br>
                      <div className="accessoriesPrice">
                        <p className="accessoriesTitle">Ціна:</p>
                        <p className="accessoriesPriceTitle">{caseData?.price}₴</p>
                      </div>
                    </div>
                  </div>
                  <LineDivider/>
                </> : <></>
                }
              </div>
            </div>
          </div>
          <br></br>
          <div style={{ display: 'flex', justifyContent: 'flex-end' }} >
            <CustomButton 
              onClick={checkFormValidation() ? () => addToCart() : () => notifyError()} 
              title='Додати вашу збірку до корзини' 
              style={checkFormValidation() ? { width: '240px', opacity: '1' } : { width: '240px', opacity: '0.5' }}
            />
          </div>
          <br></br>
          <Toaster/>
        </div>
    )
}

export default MainPage;