export const processors = [
  {
	  label: 'INTEL CORE I3', value: 'i3'
  }, 
  {
	  label: 'INTEL CORE I5', value: 'i5'
  }, 
  {
	  label: 'INTEL CORE I7', value: 'i7'
  },
  {
	  label: 'INTEL CORE I9', value: 'i9'
  },
  {
	  label: 'AMD RYZEN 3', value: 'ryzen3'
  },
  {
	  label: 'AMD RYZEN 5', value: 'ryzen5'
  },
  {
	  label: 'AMD RYZEN 7', value: 'ryzen7'
  },
  {
	  label: 'AMD RYZEN 9', value: 'ryzen9'
  },
];

export const coolers = [
  {
	  label: 'Водяне охолодження', value: 'water_cooler'
  }, 
  {
	  label: 'Повітряне охолодження', value: 'air_cooler'
  },
];

export const motherboards = [
  {
	  label: 'ASUS', value: 'ASUS'
  }, 
  {
	  label: 'MSI', value: 'MSI'
  },
  {
	  label: 'GIGABYTE', value: 'GIGABYTE'
  },
];

export const rams = [
  {
	  label: '8 GB (4 + 4)', value: '8'
  },
  {
	  label: '16 GB (8 + 8)', value: '16'
  },
  {
	  label: '32 GB (16 + 16)', value: '32'
  },
  {
	  label: '64 GB (32 + 32)', value: '64'
  },
];

export const videocards = [
  {
	  label: 'GT 1030 2GB', value: '1030'
  }, 
  {
	  label: 'GTX 1050 TI 4GB', value: '1050ti'
  }, 
  {
	  label: 'GTX 1630 4GB', value: '1630'
  }, 
  {
	  label: 'GTX 1650 4GB', value: '1650'
  }, 
  {
	  label: 'GTX 1660 Super 6GB', value: '1660'
  },
  {
	  label: 'GeForce RTX 3050 8GB', value: '3050'
  },
  {
	  label: 'GeForce RTX 3060 12GB', value: '3060'
  },
  {
	  label: 'GeForce RTX 3060 TI 8GB', value: '3060ti'
  },
  {
	  label: 'GeForce RTX 3070 TI 8GB', value: '3070ti'
  },
  {
	  label: 'GeForce RTX 4070 12GB', value: '4070'
  },
  {
	  label: 'GeForce RTX 4070 TI 12GB', value: '4070ti'
  },
  {
	  label: 'GeForce RTX 4080 16GB', value: '4080'
  },
  {
	  label: 'GeForce RTX 4090 24GB', value: '4090'
  },
  {
	  label: 'Radeon RX 550 4GB', value: '550'
  },
  {
	  label: 'Radeon RX 6500 XT 4GB', value: '6500'
  },
  {
	  label: 'Radeon RX 6600 8GB', value: '6600'
  },
  {
	  label: 'Radeon RX 6700 XT 12GB', value: '6700'
  },
  {
	  label: 'Radeon RX 6900 XT 16GB', value: '6900'
  },
  {
	  label: 'Radeon RX 7900 XTX 24GB', value: '7900'
  },
  {
	  label: 'Без відеокарти', value: 'without_videocard'
  },
];

export const hdds = [
  {
	  label: '1 TB', value: '1000'
  }, 
  {
	  label: '2 TB', value: '2000'
  },
  {
	  label: '4 TB', value: '4000'
  },
  {
	  label: '8 TB', value: '8000'
  },
  {
	  label: 'Без HDD диску', value: 'without_hdd'
  },
];

export const ssds = [
  {
	  label: '120 GB', value: '120'
  },
  {
	  label: '240 GB', value: '240'
  }, 
  {
	  label: '480 GB', value: '480'
  },
  {
	  label: '960 GB', value: '960'
  },
  {
	  label: 'Без SSD диску', value: 'without_ssd'
  },
];

export const powers = [
  {
	  label: '400W', value: '400'
  },
  {
	  label: '500W', value: '500'
  }, 
  {
	  label: '600W', value: '600'
  },
  {
	  label: '700W', value: '700'
  },
  {
	  label: '800W', value: '800'
  },
  {
	  label: '1000W', value: '1000'
  },
  {
	  label: '1200W', value: '1200'
  },
];

export const cases = [
  {
	  label: 'DeepCool', value: 'DeepCool'
  },
  {
	  label: 'NZXT', value: 'NZXT'
  }, 
  {
	  label: 'be quiet!', value: 'be quiet!'
  },
  {
	  label: 'Corsair', value: 'Corsair'
  },
  {
	  label: 'Others', value: 'Others'
  },
];

export const maxVideocard1660 = {
  processors: [
    'AMD Ryzen 3 1200AF 3200 МГц', 
    'AMD Ryzen 3 3200G 3600 МГц',
  ],
  videocards: ['1030', '1050ti', '1630', '1650', '1660', '550', '6500']
}

export const maxVideocard3050 = {
  processors: [
    'AMD Ryzen 3 4100 3800 МГц', 
    'AMD Ryzen 3 4300G 3800 МГц',
    'Intel Core i3-10105F 3700 МГц',
    'Intel Core i3-10100F 3600 МГц',
    'Intel Core i3-10105 3700 МГц',
    'Intel Core i3-10100 3600 МГц',
  ],
  videocards: [...maxVideocard1660.videocards, '3050', '6600']
}

export const maxVideocard3060ti = {
  processors: [
    'AMD Ryzen 5 3600 3600 МГц', 
    'Intel Core i3 8350K 4000 МГц',
    'Intel Core i3-12100F 3300 МГц',
    'Intel Core i3-12100 3300 МГц',
    'Intel Core i5-10400F 2900 МГц',
    'Intel Core i5-10400 2900 МГц',
    'Intel Core i5-11400F 2600 МГц ',
    'Intel Core i5-11400 2600 МГц',
    'Intel Core i7-10700F 2900 МГц',
    'Intel Core i7-11700F 2500 МГц',
  ],
  videocards: [...maxVideocard3050.videocards, '3060', '3060ti', '6700']
}

export const maxVideocard3070ti = {
  processors: [
    'AMD Ryzen 5 4500 3600 МГц', 
    'AMD Ryzen 5 4600G 3700 МГц',
    'AMD Ryzen 5 5500 3600 МГц',
    'AMD Ryzen 5 5600G 3900 МГц',
    'Intel Core i5-12400F 2500 МГц',
    'Intel Core i7-10700KF 3800 МГц',
    'Intel Core i7-10700K 3800 МГц ',
    'Intel Core i7-11700KF 3600 МГц',
    'Intel Core i9-11900K 3500 МГц',
  ],
  videocards: [...maxVideocard3060ti.videocards, '3070ti', '6700']
}

export const maxVideocard4070 = {
  processors: [
    'AMD Ryzen 5 5600X 3700 МГц',
    'AMD Ryzen 7 3800X 3900 МГц', 
    'Intel Core i5-13400F 2500 МГцц',
    'Intel Core i7-12700KF 3600 МГц',
  ],
  videocards: [...maxVideocard3070ti.videocards, '4070', '6900']
}

export const maxVideocard4070ti = {
  processors: [
    'AMD Ryzen 5 7600X 4700 МГц', 
    'AMD Ryzen 7 5800X 3800 МГц',
    'Intel Core i5-13600K 3500 МГц',
    'Intel Core i5-13600KF 3500 МГц',
    'Intel Core i7-13700K 3400 МГц',
    'Intel Core i7-13700KF 3400 МГц',
  ],
  videocards: [...maxVideocard4070.videocards, '4070ti', '6900']
}

export const maxVideocard4080 = {
  processors: [
    'AMD Ryzen 7 7700X 4500 МГц', 
    'AMD Ryzen 9 7900X 4700 МГц',
    'Intel Core i9-12900KF 3200 МГц',
  ],
  videocards: [...maxVideocard4070ti.videocards, '4080', '7900']
}

export const maxVideocard4090 = {
  processors: [
    'AMD Ryzen 9 7950X 4500 МГц', 
    'Intel Core i9-13900K 3000 МГц',
    'Intel Core i9-13900KF 3000 МГц',
  ],
  videocards: [...maxVideocard4080.videocards, '4090', '7900']
}

export const maxVideocard1660_All = {
  processors: [
    ...maxVideocard1660.processors,
    ...maxVideocard3050.processors,
    ...maxVideocard3060ti.processors,
    ...maxVideocard3070ti.processors,
    ...maxVideocard4070.processors,
    ...maxVideocard4070ti.processors,
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard1660.videocards
}

export const maxVideocard3050_All = {
  processors: [
    ...maxVideocard3050.processors,
    ...maxVideocard3060ti.processors,
    ...maxVideocard3070ti.processors,
    ...maxVideocard4070.processors,
    ...maxVideocard4070ti.processors,
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard3050.videocards
}

export const maxVideocard3060ti_All = {
  processors: [
    ...maxVideocard3060ti.processors,
    ...maxVideocard3070ti.processors,
    ...maxVideocard4070.processors,
    ...maxVideocard4070ti.processors,
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard3060ti.videocards
}

export const maxVideocard3070ti_All = {
  processors: [
    ...maxVideocard3070ti.processors,
    ...maxVideocard4070.processors,
    ...maxVideocard4070ti.processors,
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard3070ti.videocards
}

export const maxVideocard4070_All = {
  processors: [
    ...maxVideocard4070.processors,
    ...maxVideocard4070ti.processors,
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard4070.videocards
}

export const maxVideocard4070ti_All = {
  processors: [
    ...maxVideocard4070ti.processors,
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard4070ti.videocards
}

export const maxVideocard4080_All = {
  processors: [
    ...maxVideocard4080.processors,
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard4080.videocards
}

export const maxVideocard4090_All = {
  processors: [
    ...maxVideocard4090.processors,
  ],
  videocards: maxVideocard4090.videocards
}